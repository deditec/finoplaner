<?php
class LocuAPI
{
	public $_outputType = "json"; //either json, xml or array
	public $_errors = array();
		
	protected $_apiKey = '';
	protected $_apiUrl = "http://api.locu.com/v1_0";
	protected $_name;
	
	protected $_apiCallType = '';
	protected $_includeDetails = false;
	protected $_language = 'en';
	protected $_location; // Required - This must be provided as a google.maps.LatLng object.
	protected $_radius; // Required
	protected $_category;
	protected $_keywords;
	protected $_venueId;
	protected $_country;
	protected $_region;
	protected $_hasMenu;

	protected $_reference;
	protected $_accuracy;
	
	public function __construct($apiKey)
	{
		$this->_apiKey = $apiKey;	
	}

	public function search()
	{
		$this->_apiCallType = "venue/search";
		return $this->_apiCall();
	}
	
	public function show($id = NULL)
	{
		if($id){
			$this->setVenueId($id);
			$this->_apiCallType = "venue/";
			return $this->_apiCall();
		}
		
	}
	
	public function setLocation($location)
	{
		$this->_location = $location;
	}

	public function setRadius($radius)
	{
		$this->_radius = $radius;
	}

	public function setCategory($category)
	{
		$this->_category = $category;
	}
	
	/* ==== */
	public function setCountry($country){
		$this->_country = $country;
	}
	
	public function setMenu($bool){
		$this->_hasMenu = $bool;
	}

	public function setVenueId($venueId)
	{
		
		$this->_venueId = $venueId;
	}

	public function setRegion($region){
		$this->_region = $region;
	}
	/* ==== */
	public function setKeywords($keywords)
	{
		$this->_keywords = $keywords;
	}

	public function setLanguage($language)
	{
		$this->_language = $language;
	}

	public function setName($name)
	{
		$this->_name = $name;
	}

	public function setSensor($sensor)
	{
		$this->_sensor = $sensor;
	}

	public function setReference($reference)
	{
		$this->_reference = $reference;
	}

	public function setAccuracy($accuracy)
	{
		$this->_accuracy = $accuracy;
	}

	public function setIncludeDetails($includeDetails)
	{
		$this->_includeDetails = $includeDetails;
	}
	
	

	protected function _checkErrors()
	{
		if(empty($this->_apiCallType)) {
			$this->_errors[] = "API Call Type is required but is missing.";
		}

		if(empty($this->_apiKey)) {
			$this->_errors[] = "API Key is is required but is missing.";
		}

		if(($this->_outputType!="json") && ($this->outputType!="xml") && ($this->outputType!="json")) {
			$this->_errors[] = "OutputType is required but is missing.";
		}
	}
	
	protected function buildParams(){
		$params = array();
		if (!empty($this->_location)) {
			array_push($params, "location=".$this->_location);
		}
		
		if (!empty($this->_radius)) {
			array_push($params, "radius=".$this->_radius);
		}
		
		if (!empty($this->_name)) {
			array_push($params, "name=".$this->_name);
		}
		
		if (!empty($this->_category)) {
			array_push($params, "category=".$this->_category);
		}
		
		if (!empty($this->_country)) {
			array_push($params, "country=".$this->_country);
		}
		
		if (!empty($this->_region)) {
			array_push($params, "region=".$this->_country);
		}
		
		if (!empty($this->_venueId)) {
			array_push($params, $this->_venueId);
		}
		
		if (!empty($this->_hasMenu)) {
			array_push($params, "has_menu=".$this->_hasMenu);
		}
		
		return implode('&', $params);
	}

	protected function _apiCall()
	{
		$this->_checkErrors();

		
		if($this->_apiCallType=="venue/search") {
			$URLparams = $this->buildParams();
		}
		
		if($this->_apiCallType=="venue/") {
			
			$this->_apiCallType .= $this->_venueId;
			$URLparams = '';
		}
	
		$URLToCall = $this->_apiUrl."/".$this->_apiCallType."/?api_key=".$this->_apiKey."&".$URLparams;
		$result = json_decode($this->_curlCall($URLToCall), true);
		
		$result['errors'] = $this->_errors;
		
		return $result;
	}

	protected function _curlCall($url,$topost = array())
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		if (!empty($topost)) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $topost);
		}
		$body = curl_exec($ch);
		curl_close($ch);
		return $body;
	}
}
