<div class="container">
	<div class="row-fluid media-row">
		<div class="span4">
			<div class="panel">
				<h4  class="greenText">Simple to use</h4>
				<p>
					Everything in <strong class="label label-inverse"><i class="icon-wallet"></i><span class="greenText ">Cash</span>Advise</strong><span class="hide">CashAdvise</span> is focused to provide maximum usability and quick user learning process. 
					All tasks are focused on a single working pane where you can see how your cash flow plan is developing over time. 
					By using close-to-natural way of describe it, you can specify the schedule of your income and expenses. 
					No need to figure out date and time spanning or to do manual calculations to “settle” some amounts correctly. 
					<strong class="label label-inverse"><i class="icon-wallet"></i><span class="greenText ">Cash</span>Advise</strong> does it all for you. 
				</p>
			</div>
		</div>
		<div class="span4">
			<div class="panel">
				<h4  class="greenText">Powerful</h4>
				<p>
					Simple at first glance, the <strong class="label label-inverse"><i class="icon-wallet"></i><span class="greenText ">Cash</span>Advise</strong><span class="hide">CashAdvise</span><span class="hide">CashAdvise</span> product is powerful and handy financial tool. 
					You can create variety of budgeting scenarios and experiment with your financial goals. 
					By using simple set of prioritized restriction rules you can fine-tune the process of your cash distribution over time. 
					You can easily spawn new plan from a specific point in time and create new scenario for wealth grow exploratory purposes.
				</p>
			</div>
		</div>
		<div class="span4">
			<div class="panel">
				<h4  class="greenText">Visual</h4>
				<p>
					By focusing the work on a single powerful web view you can instantly see all changes they you make. 
					<strong class="label label-inverse"><i class="icon-wallet"></i><span class="greenText ">Cash</span>Advise</strong><span class="hide">CashAdvise</span> does not have complicated charts and tables. 
					Every important bit of information is always at your glance. 
					By using time-line panning and zoom options you can always check the &quot;big&quot; picture but keep eye on the small details of your cash plan.
				</p>	
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="panel">
				<h3>Easy and friendly</h3>
				<div class="row-fluid media-row">
					<div class="span6">
						<div class="row-fluid media-row">
							<div class="img-box-border span2">
								<img src="<?php echo $helpers->url_for('img/icon_pack/png256/coins.png') ?>" />
							</div>
							<div class="span10">
								<h4><a href="#">Ergonomic swim-lane workplace </a></h4>
								<p>
									The basic building blocks of your plan are lanes that show scheduled money events to your cash, credit cards, loans, savings, investments, etc.
								</p>
							</div>
						</div>
						<div class="row-fluid media-row">
							<div class="img-box-border span2">
								<img src="<?php echo $helpers->url_for('img/icon_pack/png256/pie_chart.png') ?>" />
							</div>
							<div class="span10">
								<h4><a href="#">Time-line panning and zoom</a></h4>
								<p>
									Planning is easy with time panning and zooming. This feature allows for tracking day-to-day cash model and still keeping eye on the horizon.
								</p>
						
							</div>
						</div>
						<div class="row-fluid media-row">
							<div class="img-box-border span2">
								<img src="<?php echo $helpers->url_for('img/icon_pack/png256/banknote.png') ?>" />
							</div>	
							<div class="span10">
								<h4><a href="#">Balances always visible</a></h4>
								<p>
									The important balances and amounts are displayed directly at the lanes. 
								</p>
						
							</div>
						</div>
					</div>
					<div class="span6">
						<div class="row-fluid media-row">
							<div class="img-box-border span2">
								<img src="<?php echo $helpers->url_for('img/icon_pack/png256/line_chart.png') ?>" />
							</div>
							<div class="span10">
								<h4><a href="#">Schedule and events distribution</a></h4>
								<p>Money distribution events such as income and expense actions are displayed at the according lanes by using simple color scheme. </p>
						
							</div>
						</div>
						<div class="row-fluid media-row">
							<div class="img-box-border span2">
								<img src="<?php echo $helpers->url_for('img/icon_pack/png256/gold_bullion.png') ?>" />
							</div>
							<div class="span10">
								<h4><a href="#">Rules and restrictions</a></h4>
								<p>When rules or restrictions are violated the conflict points are displayed right at the corresponding time segment of the work swim-lanes.</p>
						
							</div>
						</div>
						<div class="row-fluid media-row">
							<div class="img-box-border span2">
								<img src="<?php echo $helpers->url_for('img/icon_pack/png256/credit_card.png') ?>" />
							</div>	
							<div class="span10">
								<h4><a href="#">Multiple scenarios</a></h4>
								<p>It is very easy to create new plan by using existing as template. All plans are saved and are accessible online from your browser or mobile device.</p>
						
							</div>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>	
</div>
