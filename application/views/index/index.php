<div class="container">
	<div class="row-fluid hidden-phone">
		<div class="span12">
			<?php $this->render_partial('slideshow'); ?>
		</div>
	</div>
	<br />
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				
				<div class="span6">
					<div class="panel small border-box">
						<div class="row-fluid">
							<div class="span12">
								<p><i class="icon-instagram"></i> Cash flow management web application</p>
								<p><i class="icon-instagram"></i> Budget planning in easy and efficient way</p>
								<p><i class="icon-instagram"></i> Only $9.90!</p>
							</div>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="panel small border-box">
						<div class="row-fluid">
							<div class="span12">
								<p><i class="icon-instagram"></i> Create, analyze and store your money schedule online</p>
								<p><i class="icon-instagram"></i> Easy to access from your browser or mobile device</p>
								<p><i class="icon-instagram"></i> Keep your money flow on track</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
	<div class="row-fluid">
		<div class="span12">
			<h2 class="">
				<span class="greenText">CashAdvise</span> - cash flow management web application for only <span class="greenText">$9.90</span>! 
				Cash flow modeling in easy and efficient way. 
				Create, analyze and store your money schedule online. 
				Easy to access from your browser or mobile device.
			</h2>
			<br />
			<div class="line-light"></div>
		</div>
	</div>
	-->
	<br />
	<div class="row-fluid">
		<div class="span12">
			<div class="panel">
				<h4>Welcome!</h4>
			</div>	
		</div>
	</div>	
	<div class="row-fluid">	
		<div class="span6">
			<div class="panel">
				<div class="row-fluid">
					<div class="img-box-border text-left span12">
						<img class="img-border" src="<?php echo $helpers->url_for('img/welcome_small_graphic.jpg') ?>" />
					</div>
				</div>
				<br />
				<p>
					<strong class="label label-inverse"><i class="icon-wallet"></i><span class="greenText ">Cash</span>Advise</strong><span class="hide">CashAdvise</span> is simple yet powerful web application that allows you to create and save money management plans. 
					All data is presented in simple and visual way, easy to understand and modify. 
					Flexible options and rules allow to build the cash flow model that expresses your current income and expenses. 
					The schedule is expressed in "what-you-see" manner - 
					you can track and explore how your wealth will change in the next couple days, weeks or months.
				</p>
				<div class="row-fluid">
					<div class="span12 text-left">
						<a href="<?php echo $helpers->url_for('features') ?>" class="btn btn-green">Read more</a>
					</div>
				</div>
			</div>
			
		</div>
		<div class="span6">
			<div class="panel">
				<div class="row-fluid media-row">
					<div class="span2 text-center">
						<h4><i class="icon-ok-circled"></i></h4>
					</div>
					<div class="span10">
						<h4><a href="<?php echo $helpers->url_for('features'); ?>">Simple</a></h4>
						<p>
							Put aside the calculator and the stylesheets. 
							Build your money schedule in a matter of minutes. 
							Start to explore options for growth. 
							Instantly check when you can afford more expenses.
						</p>
					</div>
				</div>

			</div>
			<br />
			<div class="panel">
				<div class="row-fluid media-row">
					<div class="span2 text-center">
						<h4><i class="icon-ok-circled"></i></h4>
					</div>
					<div class="span10">
						<h4><a href="<?php echo $helpers->url_for('features'); ?>">Powerful</a></h4>
						<p>
							Manage the plan complexity which suits your needs. 
							Use various scheduling options and money management rules to fine tune the process.
						</p>
					</div>
				</div>

			</div>
			<br />
			<div class="panel">
				<div class="row-fluid media-row">
					<div class="span2 text-center">
						<h4><i class="icon-ok-circled"></i></h4>
					</div>
					<div class="span10">
						<h4><a href="<?php echo $helpers->url_for('features'); ?>">Visual</a></h4>
						<p>
							Most important data is always where you need it. 
							Go back and forward into your plan and you will see each balance, 
							transaction or cash event that compose your financial journal.
						</p>
					</div>
				</div>
			</div>
			<br />
			<div class="panel">
				<div class="row-fluid">
					<div class="span10 offset2 text-left">
						<a href="<?php echo $helpers->url_for('features'); ?>" class="btn btn-green">Read more</a>
					</div>
				</div>
			</div>
		</div>
		<!--
		<div class="span6">
			<div class="row-fluid media-row">
				<div class="span6">
					<div class="panel small">
						<div class="row-fluid media-row">
							<div class="span2 text-center">
								<p class="greenText"><i class="icon-instagram"></i></p>
							</div>
							<div class="span10">
								<p><a href="#">Cash flow management web application</a></p>
							</div>
						</div>
				
					</div>
				</div>
				<div class="span6">
					<div class="panel small">
						<div class="row-fluid media-row">
							<div class="span2 text-center">
								<p class="greenText"><i class="icon-instagram"></i></p>
							</div>
							<div class="span10">
								<p><a href="#">Create, analyze and store your money schedule online</a></p>
							</div>
						</div>
				
					</div>
				</div>
			</div>
			
			<div class="row-fluid media-row">
				<div class="span6">
					<div class="panel small">
						<div class="row-fluid media-row">
							<div class="span2 text-center">
								<p class="greenText"><i class="icon-instagram"></i></p>
							</div>
							<div class="span10">
								<p><a href="#">Budget planning in easy and efficient way</a></p>
							</div>
						</div>
				
					</div>
				</div>
				<div class="span6">
					<div class="panel small">
						<div class="row-fluid media-row">
							<div class="span2 text-center">
								<p class="greenText"><i class="icon-instagram"></i></p>
							</div>
							<div class="span10">
								<p><a href="#">Easy to access from your browser or mobile device</a></p>
							</div>
						</div>
				
					</div>
				</div>
			</div>
			
			<div class="row-fluid media-row">
				<div class="span6">
					<div class="panel small">
						<div class="row-fluid media-row">
							<div class="span2 text-center">
								<p class="greenText"><i class="icon-instagram"></i></p>
							</div>
							<div class="span10">
								<p><a href="#">Only $9.90!</a></p>
							</div>
						</div>
				
					</div>
				</div>
				<div class="span6">
					<div class="panel small">
						<div class="row-fluid media-row">
							<div class="span2 text-center">
								<p class="greenText"><i class="icon-instagram"></i></p>
							</div>
							<div class="span10">
								<p><a href="#">Keep your money flow on track</a></p>
							</div>
						</div>
				
					</div>
				</div>
			</div>
		</div>
		-->
	</div>		
</div>
