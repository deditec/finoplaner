<div class="container">
	<div class="row-fluid">
		<br />
		<div class="panel span12">
			<div class="row-fluid">
			<div class="span3">
				<div class="row-fluid">
					<div class="img-box-border text-center span12">
						<img class="img-border" src="<?php echo $helpers->url_for('img/Dedotek-logo-big.jpg') ?>" />
					</div>
				</div>
			</div>
			<div class="span9 text-left">
				<p>
					<strong>Dedodek Software</strong> is a small innovative company dedicated to produce simplistic web-based software products for personal and small business use. 
					We are trying to offer easy and handy applications at very attractive prices. 
					Our team is constantly exploring simplified solutions to everyday tasks. 
					We are turning these simplistic and powerful ideas into usable solutions by shaping them with leading edge and exciting web technologies.
				</p>
			</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<p>
						CashAdvise is our new endeavor to approach a classic money management problem – cash flow management and  budget modeling. 
						Everybody have some strategy how to control and track its personal finances. 
						Our solution employ  simplistic but powerful approach, very close to the person's way of thinking about its money journal. 
					</p>
					<p>
						The product primary goal is to be easy to understand and usable. 
						We do this by employing several visual techniques to  keep the complexity away - 
						<strong class="label label-inverse"><i class="icon-wallet"></i><span class="greenText ">Cash</span>Advise</strong><span class="hide">CashAdvise</span> does it all for you! 
						Your rules and scheduling options are expressed almost in natural language form and can be easily modified. 
						You can explore many scenarios and variations by playing-out what might happen if you increase your income or change your money distribution in many different ways.
					</p>
				</div>
			</div>	
		</div>
		

	</div>
</div>
