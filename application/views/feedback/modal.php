<div class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h3><span class="greenText">Feed</span>Back</h3>
	</div>
	<form id="feedback-form" class="form-horizontal" action="<?php echo $helpers->url_for('feedback/sent') ?>" data-remote="true" method="post">
		<div class="modal-body">
			
			<p>
				Sent your feedback for this web site
			</p>
			<div class="alert alert-error hide">
				
			</div>
			<fieldset>
				<legend class="greenText">
					Your Contacts:
				</legend>
				
				 <div class="control-group">
    				<label class="control-label" for="inputName">Name: </label>
    				<div class="controls">
      					<input type="text" id="inputName" name="value[name]" placeholder="Name" value="<?php echo (isset($current_user['name']) && !empty($current_user['name']) )? $current_user['name']:''; ?>">
    				</div>
  				</div>
  				 <div class="control-group">
    				<label class="control-label" for="inputName">Email: </label>
    				<div class="controls">
      					<input type="email" id="inputEmail" name="value[email]" placeholder="Email" value="<?php echo (isset($current_user['email']) && !empty($current_user['email']) )? $current_user['email']:''; ?>">
    				</div>
  				</div>
			</fieldset>
			<fieldset>
				<legend class="greenText">
					Message:
				</legend>
				 <div class="control-group">
    				<label class="control-label" for="inputSubject">Subject: </label>
    				<div class="controls">
      					<input type="text" id="inputSubject" name="value[subject]" placeholder="Subject">
    				</div>
  				</div>
  				<div class="control-group">
  					<label class="control-label" for="inputMessageText">Text: </label>
					<div class="controls">
						<textarea id="inputMessageText" class="input-large" name="value[text]"></textarea>
					</div>
				</div>
			</fieldset>
			
			
		
	</div>

	<div class="modal-footer">
		<div class="row-fluid">
				<div class="span12 text-right">
					<?php echo $helpers->csrf()?>
					<button type="submit" class="btn btn-green" > Sent </button>
				</div>
			</div>
	</div>
	</form>
</div>
