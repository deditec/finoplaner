<div class="row-fluid">
	<div class="span12">
		<div class="panel">
			<h4>What information do we collect <i class="icon-help"></i></h4>
			<p>We collect information from you when you register on our site. </p>
			<p>
				When ordering or registering on our site, as appropriate, you may be asked to enter your: name or e-mail address. 
				You may, however, visit our site anonymously.
			</p>
			<br />
			<h4>What do we use your information for <i class="icon-help"></i></h4>
			<p>
				Any of the information we collect from you may be used in one of the following ways: 
			</p>
			<p>
				* To personalize your experience (your information helps us to better respond to your individual needs)
			</p>
			<p>
				* To improve our website
				(we continually strive to improve our website offerings based on the information and feedback we receive from you)
			</p>
			<p>
				* To improve customer service
				(your information helps us to more effectively respond to your customer service requests and support needs)
			</p>
			<p>* To send periodic emails</p>
			<p>
				The email address you provide for order processing, will only be used to send you information and updates pertaining to your order.
			</p>
			<br />
			<h4>Do we use cookies <i class="icon-help"></i></h4>
			<p>We do not use cookies.</p>
			<br />
			<h4>Do we disclose any information to outside parties <i class="icon-help"></i></h4>
			<p>
				We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. 
				This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. 
				We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. 
				However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.
			</p>
			<br />
			<h4>Online Privacy Policy Only </h4>
			<p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>
			<br />
			<h4>Your Consent</h4>
			<p>By using our site, you consent to our web site privacy policy.</p>
			<br />
			<h4>Changes to our Privacy Policy</h4>
			<p>If we decide to change our privacy policy, we will send an email notifying you of any changes.</p>
			<br />
			<h4>Contacting Us</h4>
			<p>If there are any questions regarding this privacy policy you may <a href="<?php echo $helpers->url_for('contact') ?>">contact us</a> using the information below.</p> 
			<address>
				<strong>
					<a href="<?php echo $helpers->url_for() ?>" title="www.cashadvise.co"><span class="label label-inverse">www.<i class="icon-wallet"></i><span class="greenText ">Cash</span>Advise.co</span></a>
					<br />
					<span>42 Parensov Str.</span>
					<br />
					<span>Sofia 1142</span>
					<br />
					<span>Bulgaria </span>
				</strong>
			</address>
		</div>
	</div>
	
</div>