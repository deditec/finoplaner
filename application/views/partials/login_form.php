<div class="row-fluid login-form">
	<form id="login-form" class="form-horizontal span12" data-remote="true" method="post" action="<?php echo $helpers->url_for('user/login') ?>">
		<?php echo $helpers->csrf()?>
		<fieldset>
			<legend>
				
				<div class="row-fluid">
					<div class="span6 greenText">
						<i class="icon-users"></i> Login:
					</div>
					<div class="span6 text-right">
						<small> or <a href="javascript:;" class="showContent" data-show="signup-form" data-hide="login-form"><i class="icon-user-add"></i> Sign up</a></small>
					</div>
				</div>
			</legend>
			<div class="alert alert-error hide">
				
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Email: </label>
				<div class="controls">
					<input type="email" name="email" id="inputEmail" placeholder="Email">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Password: </label>
				<div class="controls">
					<input type="password" name="password" id="inputPass" placeholder="Password">
					
				</div>
			</div>
			<div class="row-fluid">
	
				<div class="span12 text-right">
					<small><a href="javascript:;" class="showContent" data-show="forgote-password-form" data-hide="login-form" >forgot password <i class="icon-help"></i></a> or </small>
					<button type="submit" class="btn btn-green btn" >
						Go <i class="icon-right-circled"></i>
					</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<div class="row-fluid signup-form hide">
	<form id="signup-form" class="span12 form-horizontal" data-remote="true" method="post" action="<?php echo $helpers->url_for('user/signup') ?>">
		<?php echo $helpers->csrf()?>
		<fieldset>
			<legend>

				<div class="row-fluid">
					<div class="span6 greenText">
						<i class="icon-user-add"></i> Create account:
					</div>
					<div class="span6 text-right">
						<small> or <a href="javascript:;" class="showContent" data-show="login-form" data-hide="signup-form"><i class="icon-users"></i> Login</a></small>
					</div>
				</div>
			</legend>
			<div class="alert alert-error hide">
			
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Email: </label>
				<div class="controls">
					<input type="email" id="inputEmail" name="value[email]" placeholder="Email">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Password: </label>
				<div class="controls">
					<input type="password" id="inputPass" name="value[password]" placeholder="Password">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Confirm Password: </label>
				<div class="controls">
					<input type="password" id="inputPass" name="value[confirm_password]" placeholder="Repeat password">
				</div>
			</div>
			<div class="hide privacypolicy_text">
				<fieldset >
					<legend class="">
						Privacy and Policy
						<a href="#" class="btn btn-mini btn-green showContent pull-right"  data-hide="privacypolicy_text">Close</a>
					</legend>
					<?php $this->render_partial('privacypolicy_text'); ?>
					<a href="#" class="btn btn-mini btn-green showContent pull-right"  data-hide="privacypolicy_text">Close</a>
					<br />
					<br />	
				</fieldset>	
			</div>
			<div class="row-fluid">

				<div class="span9 text-left">
					
				<label class="checkbox">
					 <input type="checkbox" name="value[agree]">  
					 <small>
					 I have read and agree to <a href="javascript:;" class="showContent" data-show="privacypolicy_text"  rel="nofollow" >Terms and conditions</a>
					</small>
				</label>
			</div>	
			<div class="span3 text-right">
					<button type="submit" class="btn btn-green btn" >
						Create <i class="icon-user-add"></i>
					</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<div class="row-fluid forgote-password-form hide">
	<form id="forgote-password-form" class="span12 form-horizontal" data-remote="true" method="post" action="<?php echo $helpers->url_for('user/reset_password') ?>">
		<?php echo $helpers->csrf()?>
		<fieldset>
			<legend>
				<div class="row-fluid">
					<div class="span6 greenText">
						<i class="icon-key"></i> Reset Password:
					</div>
					<div class="span6 text-right">
						<small> or <a href="javascript:;" class="showContent" data-show="signup-form" data-hide="forgote-password-form"><i class="icon-user-add"></i> Sign up</a></small>
					
					</div>
				</div>
			</legend>
			<div class="alert alert-error hide">
			
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Email: </label>
				<div class="controls">
					<input type="email" id="inputEmail" name="value[email]" placeholder="Email">
				</div>
			</div>
			<div class="row-fluid">

				<div class="span12 text-right">
					<button type="submit" class="btn btn-green btn" >
						Restore <i class="icon-key"></i>
					</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>