<div class="row-fluid editUser-form">
	<form id="editUser-form" class="span12 form-horizontal" data-remote="true" method="post" action="<?php echo $helpers->url_for('user/save') ?>">
		<?php echo $helpers->csrf()
		?>
		<input type="hidden" name="userID" value="<?php echo $current_user['id']?>"/>
		<fieldset>
			<legend>
				<div class="row-fluid">
					<div class="span6 greenText">
						<i class="icon-user"></i> Edit account:
					</div>
					<div class="span6 text-right">
						<small><a href="javascript:;" class="showContent" data-show="chanagePassword-form" data-hide="editUser-form">Change password <i class="icon-key"></i> </a> </small>
					</div>
				</div>
			</legend>
			<div class="alert alert-error hide">

			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Email: </label>
				<div class="controls">
					<input type="email" id="inputEmail" name="value[email]" placeholder="Email" value="<?php echo isset($current_user['email'])? $current_user['email']:'' ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Name: </label>
				<div class="controls">
					<input type="text" id="inputName" name="value[name]" placeholder="Name" value="<?php echo isset($current_user['name'])? $current_user['name']:'' ?>">
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 text-right">
					<button type="submit" class="btn btn-green btn" >
						<i class="icon-user-add"></i> Edit
					</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<div class="row-fluid chanagePassword-form hide">
	<form id="chanagePassword-form" class="span12 form-horizontal" data-remote="true" method="post" action="<?php echo $helpers->url_for('user/save') ?>">
		<?php echo $helpers->csrf()
		?>
		<input type="hidden" name="userID" value="<?php echo $current_user['id']?>"/>
		<fieldset>
			<legend>
				<div class="row-fluid">
					<div class="span8 greenText">
						<i class="icon-key"></i> Change password:
					</div>
					<div class="span4 text-right">
						<small><a href="javascript:;" class="showContent" data-show="editUser-form" data-hide="chanagePassword-form">Edit Account <i class="icon-user"></i> </a> </small>
					</div>
				</div>
			</legend>
			<div class="alert alert-error hide">

			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Old Password: </label>
				<div class="controls">
					<input type="password" id="inputPass" name="value[old_password]" placeholder="Password">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">New Password: </label>
				<div class="controls">
					<input type="password" id="inputPass" name="value[password]" placeholder="Password">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputName">Confirm Password: </label>
				<div class="controls">
					<input type="password" id="inputPass" name="value[confirm_password]" placeholder="Repeat password">
				</div>
			</div>
			<div class="row-fluid">

				<div class="span12 text-right">
					<button type="submit" class="btn btn-green btn" >
						<i class="icon-user-add"></i> Edit
					</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>