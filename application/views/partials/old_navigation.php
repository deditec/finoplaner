<div class="navbar">
	<div class="navbar-inner">
			<?php $this->render_partial('account_action_box'); ?>
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
			<a class="brand" href="<?php echo $helpers->url_for(''); ?>">
				<!--<img src="<?php echo $helpers -> url_for('img/v3.png'); ?>" />	-->
				
				<span class="large-part">
					<span><i class="icon-wallet"></i></span><span class="greenText">Cash</span><span>Advise</span>
				</span>
				<span class="small-part">Money management is now simple</span>		
				<span class="hide">CashAdvise</span>
			
			</a>
			
			<div class="nav-collapse collapse">
				<ul class="nav pull-right">
					<li <?php echo ($sitePlace === 'home')? 'class="active"':'' ?> >
						<a href="<?php echo $helpers->url_for(''); ?>">Home</a>
					</li>
					<li <?php echo ($sitePlace === 'features')? 'class="active"':'' ?> >
						<a href="<?php echo $helpers->url_for('features'); ?>">Features</a>
					</li>
					<li <?php echo ($sitePlace === 'plan')? 'class="active"':'' ?> >
						<a href="<?php echo $helpers->url_for('plan'); ?>">Try It</a>
					</li>
					
					<li class="dropdown <?php echo ($sitePlace === 'contact' || $sitePlace === 'howto' || $sitePlace === 'about')? 'active':'' ?>" >
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">More <i class="icon-instagram"></i></a>
						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo $helpers->url_for('statictext/privacypolicy'); ?>" class="summon-modal" data-method="post" data-remote="true" ><i class="icon-police"></i> Privacy Policy</a>
							</li>
							<li>
								<a href="<?php echo $helpers->url_for(''); ?>"><i class="icon-help"></i> FAQ</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo $helpers->url_for('about'); ?>"><i class="icon-user"></i> About</a>
							</li>
							<li>
								<a href="<?php echo $helpers->url_for('contact'); ?>"><i class="icon-mail"></i> Contact</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
	</div>
</div>