<header>
	<div class="row-fluid media-row">
		<div class="span6">
			<a class="brand" href="<?php echo $helpers->url_for(''); ?>">
				<!--<img src="<?php echo $helpers -> url_for('img/v3.png'); ?>" />	-->
				
				<span class="large-part">
					<span><i class="icon-wallet"></i></span><span class="greenText">Cash</span><span>Advise</span>
				</span>	
				<span class="hide">CashAdvise</span>
			
			</a>
		</div>
		<div class="span6">
			<?php $this->render_partial('account_action_box'); ?>
		</div>
	</div>
</header>
<nav class="hidden-phone hidden-tablet">
	<div class="row-fluid ">
		<div class="span12 text-center">
			<a href="<?php echo $helpers->url_for(''); ?>" class="btn btn-dark btn-large <?php echo ($sitePlace === 'home')? 'active':'' ?>">Home</a>
			<a href="<?php echo $helpers->url_for('features'); ?>" class="btn btn-dark btn-large <?php echo ($sitePlace === 'features')? 'active':'' ?>">Features</a>
			<a href="<?php echo $helpers->url_for('plans'); ?>" class="btn btn-dark btn-large <?php echo ($sitePlace === 'plans')? 'active':'' ?>">Try It</a>
			<div class="btn-group">
			 	<a data-toggle="dropdown" class="dropdown-toggle btn btn-dark btn-large <?php echo ($sitePlace === 'contact' || $sitePlace === 'howto' || $sitePlace === 'about')? 'active':'' ?>" > More <i class="icon-instagram"></i></a>
			 	<ul class="dropdown-menu text-left">
					<li>
						<a href="<?php echo $helpers->url_for('statictext/privacypolicy'); ?>" class="summon-modal" data-method="post" data-remote="true" ><i class="icon-police"></i> Privacy Policy</a>
					</li>
					<li>
						<a href="<?php echo $helpers->url_for(''); ?>"><i class="icon-help"></i> FAQ</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="<?php echo $helpers->url_for('about'); ?>"><i class="icon-user"></i> About</a>
					</li>
					<li>
						<a href="<?php echo $helpers->url_for('contact'); ?>"><i class="icon-mail"></i> Contact</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>