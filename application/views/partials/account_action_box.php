
<div class="account-box text-right">
	<?php if (isset($current_user) && !empty($current_user)):  ?>
	<small>
		<a href="<?php echo $helpers->url_for('user/modal') ?>" class="summon-modal btn btn-green btn-mini" data-remote="true" data-method="post" title="<?php echo (isset($current_user['name']) && !empty($current_user['name']) )? $current_user['name']:$current_user['email'] ?>"><i class="icon-user"></i> <?php echo (isset($current_user['name']) && !empty($current_user['name']) )? $current_user['name']:$current_user['email'] ?></a> 
		<a href="<?php echo $helpers->url_for('user/logout') ?>" class="summon-modal btn btn-danger btn-mini" title="logout"><i class="icon-logout"></i> Logout</a>
	</small>
	<?php else: ?>
	<small>
		<a href="<?php echo $helpers->url_for('user/modal') ?>" class="summon-modal btn btn-green btn-mini" data-remote="true" data-method="post" title="Login"><i class="icon-users"></i> Login</a>
		or
		<a href="<?php echo $helpers->url_for('user/modal') ?>" class="summon-modal btn btn-info btn-mini" data-remote="true" data-method="post" title="Login"><i class="icon-user-add"></i> Sign Up</a>
	</small>
	<?php endif; ?>
</div>