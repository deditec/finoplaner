<div class="carousel-box">
	<!--<small>BETA version</small>
	<div class="label label-important pull-right">
 		 Comming soon!
	</div>
	-->
	<div id="myCarousel" class="carousel slide">
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active">1</li>
			<li data-target="#myCarousel" data-slide-to="1">2</li>
			<li data-target="#myCarousel" data-slide-to="2">3</li>
			<li data-target="#myCarousel" data-slide-to="3">4</li>
			<li data-target="#myCarousel" data-slide-to="4">5</li>
		</ol>
		<!-- Carousel items -->
		<div class="carousel-inner" data-amimate="test">
			<div class="item active">
				<div class="carousel-caption no_bg goLeft panel">
					<h4 class="greenText">Money management is now simple!</h4>
					<br />
					<p>
						Quickly create your cash flow model to explore wealth grow opportunities.
					</p>
					
				</div>	
				<!-- img has been create by gumzz -->
				<img src="<?php echo $helpers -> url_for('img/slider/7.jpg'); ?>" />
				<div class="button-slider-box goRight">
					<?php if (!isset($current_user) && empty($current_user)):  ?>
						<div>
							<a href="<?php echo $helpers->url_for('user/modal') ?>" class="summon-modal btn btn-green" data-remote="true" data-method="post" title="Join">Join</a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="<?php echo $helpers->url_for('features') ?>" class="btn btn-info" title="Find more">Find out more</a>
						</div>
						<div>
							<p><small>Already a member? <a href="<?php echo $helpers->url_for('user/modal') ?>" class="summon-modal" data-remote="true" data-method="post" title="Sign up"><i class="icon-user-add"></i> Sign up</a></small></p>
						</div>
					<?php endif; ?>
				</div>
				<div class="carousel-caption">
					<h4>Friendly and affordable solution</h4>
					<br />
					<p>
						Web-based cash flow management software for only $9.90.
					</p>
					
				</div>
			</div>
			<div class="item">
				
				<img src="<?php echo $helpers -> url_for('img/slider/1.jpg'); ?>" />
				
				
				<div class="carousel-caption">
					<h4 class="greenText">Quick modelling</h4>
					<br />
					<p>
						Simple steps to create a schedule when and how your income and expenses take place. 
					</p>
				</div>
			</div>
			<div class="item">
				<img src="<?php echo $helpers -> url_for('img/slider/3.jpg'); ?>" />
				<div class="carousel-caption">
					<h4>"What-you-see" data presentation</h4>
					<br />
					<p>
						No more tables and complex charts. What you see is what really matters - balance, cash flow and money distribution.
					</p>
				</div>
			</div>
			<div class="item">
				<img src="<?php echo $helpers -> url_for('img/slider/2.jpg'); ?>" />
				<div class="carousel-caption">
					<h4>Driven by your rules</h4>
					<br />
					<p>
						Powerful and smart rule management. Make it your way. Verify your finance goals and expectations right now.
					</p>
				</div>
			</div>
			<div class="item">
				<img src="<?php echo $helpers -> url_for('img/slider/4.jpg'); ?>" />
				<div class="carousel-caption">
					<h4>Instant plan changes</h4>
					<br />
					<p>
						Each time you make a change your plan is re-shaped right now. Save more than one scenario. Experiment with different expense and income schedules. 
					</p>
				</div>
			</div>
		</div>
		<!-- Carousel nav -->
		<!--
		<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
		-->
	</div>
</div>	
