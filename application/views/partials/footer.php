<footer>
	<div class="container hidden-phone">
		<div class="row-fluid media-row">
			<div class="span3 text-center">
				<div class="panel text-left">
				<ul class="social">
					<li>
						<a href="#" class="fb_button social-button"><i class="icon-facebook"></i>Facebook</a>
					</li>
					
					<li>
						<a href="#" class="twitter_button social-button"><i class="icon-twitter"></i>Twitter</a>
					</li>
					<li>
						<a href="" class="linkedin_button social-button"><i class="icon-linkedin"></i>Linkedin</a>
					</li>
					<li>
						<a href="" class="gplus_button social-button"><i class="icon-gplus"></i>Google +</a>
					</li>
				</ul>
				</div>
			</div>
			<div class="span6">
				<div class="panel">
					<h4>Money management is now simple!</h4>
					<p>
						Start today, sign in and create your cash flow model in a matter of minutes. 
						Experiment with schedule options and cash restriction rules. 
						Adjust your money plan to your expectations. 
						Explore wealth grow scenarios and optimize your spending. 
						<a href="<?php echo $helpers->url_for('user/modal'); ?>" class="summon-modal" data-remote="true" data-method="post">Join now</a> For only $9.90!	
					</p>
				</div>
			</div>
			<div class="span3 hidden-phone">
				<div class="panel">
					<div class="row-fluid media-row">
						<div class="span12">
							<h5><i class="icon-ok-circled"></i>&nbsp;&nbsp;&nbsp;<a href="<?php echo $helpers->url_for('statictext/privacypolicy'); ?>" class="summon-modal" data-method="post" data-remote="true" >Privacy Policy</a></h5>
							<br />
							<h5><i class="icon-ok-circled"></i>&nbsp;&nbsp;&nbsp;<a href="<?php echo $helpers->url_for(''); ?>">FAQ</a></h5>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="line-dark"></div>
	</div>
	
	<div class="row-fluid go-down">
			<div class="span12 text-center">
				&copy; 2013 Dedotek Software
			</div>
		</div>
</footer>