<div class="container">
		
	<div class="row-fluid">
		<div class="span9">
			<div class="panel">
				
					<div class="row-fluid">
						<div class="span6 text-left">
							<h3>My Plans</h3>
							<?php if(!$authenticated){ ?>
							<div class="alert alert-warning">
									This is demo!
									For full optionality please 
										<a href="<?php echo $helpers->url_for('user/modal') ?>" class="summon-modal" data-remote="true" data-method="post"> login </a> 
										or 
										<a href="<?php echo $helpers->url_for('user/modal') ?>" class="summon-modal" data-remote="true" data-method="post">sign up!</a>
							</div>
							<?php } ?>	
						</div>
						<?php if($authenticated){ ?>
							<br />
							<div class="span6 text-right">
								<a href="<?php echo $helpers->url_for('plans/action_view') ?>" class="summon-modal btn btn-green" title="New" data-remote="true" data-method="post"><i class="icon-plus-circle"></i> New</a>
							</div>
						<?php } ?>	
					</div>
				<br />
				<div class="row-fluid">
					<div class="span12">
						<?php 
						if(isset($plans) && !empty($plans))
						{
							echo $helpers->print_plans($plans, $authenticated);
						}
						?>
					</div>
				</div>
				
				
			</div>
			
		</div>
		<div class="span3">
			
		</div>
	</div>	
</div>