<div class="modal hide fade">
	<div class="modal-body">
		<h3><span class="greenText"><?php echo ( isset($plans) && !empty($plans) )? 'Edit':'Create';  ?> Plan</span></h3>
		<form id="plan-form" class="form-horizontal" action="<?php echo $helpers->url_for('plans/save') ?>" data-remote="true" method="post">
			<div class="row-fluid">
				 <div class="span6">
					<label class="help-inline" for="inputName">Title: </label>
	  				<input type="text" id="inputName" name="value[name]" placeholder="My Plan" value="<?php echo (isset($plans['name']) && !empty($plans['name']))? $plans['name']:''  ?>">
					
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 text-right">
					<?php echo $helpers->csrf()?>
					<?php if(isset($plans['id']) && !empty($plans['id'])) {?>
						<input type="hidden" name="value[id]" value="<?php echo $plans['id'] ?>" />
					<?php }?>
					<button type="submit" class="btn btn-green" > Save </button>
				</div>
			</div>
		</form>
	</div>
</div>
