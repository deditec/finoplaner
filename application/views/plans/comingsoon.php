<div class="container">
	<div class="row-fluid">
		<br />
		<div class="panel">
			<div class="img-box-border span6">
				<img src="<?php echo $helpers->url_for('img/try-it-logo.jpg') ?>" />
			</div>
			<div class="span6 text-center">
				<h4 class="">Our application is coming very soon...</h4>
				<h4>You will be referred back to the site's home page</h4>
				<a href="<?php echo $helpers->url_for(''); ?>" class="btn btn-large btn-green">Back&nbsp;&nbsp;<i class="icon-right-circle-1"></i> </a>
			</div>
		</div>

	</div>
</div>
