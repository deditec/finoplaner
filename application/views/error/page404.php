<br />
<div class="alert alert-warning text-center">ERROR 404: PAGE NOT FOUND</div>
<div class="container">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="text-center">EVER FEEL LIKE YOU`RE IN THE WRONG PLACE ?</h3>
		</div>
	</div>
	<br />
	<br />
	<div class="row-fluid">
			<div class="img-box  span12">
				<img class="img-border" src="<?php echo $helpers->url_for('img/404.png') ?>" />
			</div>
	</div>
	<div class="row-fluid">
		<div class="span12 text-center">
			<a href="<?php echo $helpers->url_for('') ?>" class="btn btn-large btn-green"> BACK </a>
		</div>	
	</div>	
</div>			