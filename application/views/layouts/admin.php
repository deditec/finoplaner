<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<title>Deditec</title>
		<meta charset="UTF-8" />
		<meta name="google" value="notranslate">
		<meta http-equiv="Content-Language" content="en" />
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<?php echo $helpers->include_css('bootstrap'); ?>
		<?php echo $helpers->include_css('base'); ?>
		<?php echo $helpers->include_css('app'); ?>
	</head>
	<body class="">
		<div class="container">
			<?php echo $this->content; ?>
		</div>
		<script>
			var BASE_URL = "<?php echo BASE_URL; ?>", CDN_BASE_URL = "<?php echo CDN_URL; ?>";
		</script>
		<?php echo $helpers->include_js('jquery-1.9.1.min'); ?>
		<?php echo $helpers->include_js('bootstrap') ?>
		<?php echo $helpers->include_js('ujs'); ?>
		<?php echo $helpers->include_js('base'); ?>
		<?php echo $helpers->include_js('app'); ?>
	</body>
</html>
