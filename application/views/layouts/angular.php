<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<title>CashAdvise</title>
		<meta charset="UTF-8" />
		<meta name="google" value="notranslate">
		<meta http-equiv="Content-Language" content="en" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="fluid-icon" href="<?php echo $helpers->url_for('img/coins.png') ?>" title="CashAdvise">
		<link rel="icon" type="image/x-icon" href="<?php echo $helpers->url_for('img/coins.png') ?>">
		<?php echo $this->render_partial('keyword'); ?>
		<?php echo $helpers->include_css('bootstrap'); ?>
		<?php echo $helpers->include_css('bootstrap-responsive'); ?>
		<?php echo $helpers->include_css('fix_bootstrap'); ?>
		<?php echo $helpers->include_css('fontello'); ?>
		<?php echo $helpers->include_css('base'); ?>
		<?php echo $helpers->include_css('app'); ?>
		<!--[if lt IE 7]><?php echo $helpers->include_css('fix_ie'); ?><![endif]-->
		<!--[if lt IE 8]><?php echo $helpers->include_css('fix_ie'); ?><![endif]-->
		<!--[if lt IE 9]><?php echo $helpers->include_css('fix_ie'); ?><![endif]-->
		<!--[if lt IE 10]><?php echo $helpers->include_css('fix_ie'); ?><![endif]-->
	</head>
	<body class="">
		<div id="page-wrapper">
			<?php $this ->render_partial('header'); ?>
			<div ng-app id="page-content">
				<a href="<?php echo $helpers->url_for('feedback/modal') ?>" class="feedback-button btn btn-green hidden-phone summon-modal" data-remote="true" data-method="post"><i class="icon-megaphone"></i>Feedback</a>
				
				<a href="<?php echo $helpers->url_for('user/modal') ?>" class="account-button btn btn-green hidden-phone hidden-tablet summon-modal" data-remote="true" data-method="post"><i class="icon-user"></i> Account</a> 
				
				<?php echo $this->content; ?>
			</div>
		
			<?php $this->render_partial('footer');?>
		</div>	
		<div class="modal-container hide"></div>
		<script>
			var BASE_URL = "<?php echo BASE_URL; ?>", CDN_BASE_URL = "<?php echo CDN_URL; ?>";
		</script>
		<?php echo $helpers->include_js('jquery-1.9.1.min'); ?>
		<?php echo $helpers->include_js('angular.min'); ?>
		<?php echo $helpers->include_js('bootstrap') ?>
		<?php echo $helpers->include_js('ujs'); ?>
		<?php echo $helpers->include_js('app_angular'); ?>
		<?php echo $helpers->include_js('app'); ?>
	</body>
</html>
