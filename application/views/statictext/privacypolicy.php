<div class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h3><span class="greenText">Privacy</span> Policy</h3>
	</div>
	
		<div class="modal-body">
			<?php $this->render_partial('privacypolicy_text'); ?>
		</div>

		<div class="modal-footer">
			<div class="row-fluid">
				<div class="span12 text-right">
					<button class="btn btn-green" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
</div>
