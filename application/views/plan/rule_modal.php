<div class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h3><?php echo $ModalTitle ?></h3>
	</div>
	
		<div class="modal-body">
			<form class="">
			  <div class="row-fluid">
			  	<div class="span6">
			  		<label class="control-label" for="inputEmail">Title:</label>
			      	<input type="text" id="inputEmail" name="ruleTitle" placeholder="Title">
			  	</div>
			  	<div class="span6">
			  		<label class="control-label" >Type:</label>
					<label class="radio">
						<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
						Get
					</label>
					<label class="radio">
						<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
						Pay
					</label>
					<span class="help-inline">Money</span>
				</div>
			    
			  </div>
			 </form> 
		</div>

		<div class="modal-footer">
			<div class="row-fluid">
				<div class="span12 text-right">
					<button class="btn btn-green" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
</div>
