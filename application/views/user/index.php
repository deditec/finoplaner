<div class="container">
	<div class="row-fluid">
		<?php if(!isset($current_user) && empty($current_user)): ?>
		<div class="span6">
			<div class="panel border-box">
				
					<?php $this->render_partial('login_form');?>
				
			</div>
		</div>
		<?php else: ?>
		<div class="span6">
			<div class="panel">
				<h3>Hello<?php echo isset($current_user['name'])? ', '.$current_user['name']:'' ?></h3>
				<?php 
					if (isset($current_user['activate']))
					{
						echo '<p>Your account has been activate at: <span class="label label-green">'.date('d M Y, g:i a', strtotime($current_user['activate'])).'</span></p>';
					}
					else 
					{
						echo '<p>Your account is <span class="label label-warning">not activate!</span> Place open your email and follow the link!</p>'; 
					}
				?>
				<p>Your status is: <span class="label label-green"><?php echo isset($current_user['typename'])? $current_user['typename']:'' ?></span> !
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="panel border-box">
						<?php $this->render_partial('edituser_form');?>
						
					</div>
				</div>
			</div>
		</div>
		<div class="span6">
			<div class="panel">
				<div class="row-fluid">
					<div class="span12 text-right">
						<a href="<?php echo $helpers->url_for('user/logout') ?>" class="btn btn-danger"><i class="icon-logout"></i> Logout</a>
					</div>
				</div>	
			</div>
		</div>	
		<?php endif; ?>
	</div>
</div>