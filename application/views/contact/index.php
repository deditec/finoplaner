<div class="container">
	<br />	 
	<br />	 
	<div class="row-fluid">
		<div class="span5">
			<div class="panel">
				<div class="row-fluid media-row">
					<div class="img-box-border span6">
						<a href="#"><img src="<?php echo $helpers->url_for('img/address.png') ?>" /></a>
					</div>
					<div class="span6">			
						<address>
							<strong>47 Parensov Str. 1142 Sofia, Bulgaria</strong>
							<br />
							<br />
							<div class="row-fluid media-row">
								<div class="span12">
									Email: <a href="mailto:info@cashadvise.co">info@cashadvise.co</a>
								</div>
							</div>
						</address>
					</div>
				</div>	
				<br />
			</div>	
		</div>
		<div class="span7 ">
			<div class="panel border-box">
				<div class="row-fluid">
					<form id="contact-form" class="span12 form-horizontal" action="<?php echo $helpers->url_for('contact/sent') ?>" data-remote="true" method="post">
						<fieldset>
							<div class="alert alert-error hide">
				
							</div>
							<legend class="greenText">
							Your Contacts:
							</legend>
							<div class="control-group">
								<label class="control-label" for="inputName">Name: </label>
								<div class="controls">
									<input type="text" id="inputName" name="value[name]" class="span12" placeholder="Name" value="<?php echo (isset($current_user['name']) && !empty($current_user['name']) )? $current_user['name']:''; ?>" >
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="inputName">Email: </label>
								<div class="controls">
									<input type="email" id="inputEmail" name="value[email]" class="span12" placeholder="Email" value="<?php echo (isset($current_user['email']) && !empty($current_user['email']) )? $current_user['email']:''; ?>">
								</div>
							</div>
						</fieldset>	
						<fieldset>
							<legend class="greenText">
								Message:
							</legend>
							<div class="control-group">
								<label class="control-label" for="inputSubject">Subject: </label>
								<div class="controls">
									<input type="text" id="inputSubject" name="value[subject]" class="span12" placeholder="Subject">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="inputMessageText">Text: </label>
								<div class="controls">
									<textarea id="inputMessageText" class="span12" name="value[text]"></textarea>
								</div>
							</div>
						</fieldset>
						<div class="row-fluid">
							<div class="span12 text-right">
								<?php echo $helpers->csrf()?>
								<button type="submit" class="btn btn-green btn-large" >
									Sent&nbsp;&nbsp;<i class="icon-email"></i>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
