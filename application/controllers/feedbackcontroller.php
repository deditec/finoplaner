<?php
namespace Application\Controllers;

use \Application\Models;
use \Exception;

class FeedbackController extends \Core\AuthenticatedController
{

	function index()
	{
		$this->layout_as('ajax');
		$this->set('sitePlace', 'feedback');
		//$this->_template->helpers->debug($texts);
	}
	
	function modal()
	{
		$this->layout_as('ajax');
		$this->set('sitePlace', 'feedback');
		//$this->_template->helpers->debug($texts);
	}
	
	function sent()
	{
		$this->layout_as('ajax');
		if ($this->is_known_form() && $this->post_is_known())
		{
			$sender = new  \Application\Models\Sentemail();
			$mailHelper = new  \Application\Helpers\SentmailHelper();
			try
			{
				$sender->send_feedback($_POST);
				$modal = $mailHelper->feedback_succsess_modal();
				echo json_encode(array('error'=>FALSE, 'message' => 'SUCCSESS!', 'modal' => $modal ));
			}
			catch (Exception $e)
			{
				echo json_encode(array('error'=>TRUE, 'message' => $e->getMessage()));
			}
		}	
	}
	
}
