<?php
namespace Application\Controllers;

use Application\Models\Index;

class ErrorController extends \Core\AuthenticatedController
{
	function page($code = 404)
	{
		$this->set('sitePlace', 'Error')->set('slideshow', FALSE);
		$this->render('error','page'.$code);
		//$this->_template->helpers->debug($texts);
	}
}
