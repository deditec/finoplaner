<?php
namespace Application\Controllers;

use Application\Models\Index;

class IndexController extends \Core\AuthenticatedController
{
	function index()
	{
		$this->set('sitePlace', 'home')->set('slideshow', TRUE);
		//$this->_template->helpers->debug($texts);
	}
}
