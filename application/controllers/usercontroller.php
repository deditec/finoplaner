<?php
namespace Application\Controllers;

use \Application\Models;
use \Core\AuthenticatedController;
use \Exception;


class UserController
	extends AuthenticatedController
{
	
	function index()
	{
		$this->set('sitePlace', 'user');
	}
	
	function modal()
	{
		$this->set('sitePlace', 'user');
	}
	
	function signup()
	{
		$this->layout_as('ajax');
		$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
		$is_authenticated = $authenticate->check_user_data();
		if ($this->is_known_form() && !$is_authenticated)
		{
			$data = array();
			$userModel = new \Application\Models\User();
			
			try
			{
				$ok = $userModel->save_user($_POST);
				foreach($_POST['value'] as $key=>$value)
				{
					$data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
				}
				$is_authenticated = $authenticate->authenticate($data, FALSE);
				if($is_authenticated)
				{
					$sender = new \Application\Models\Sentemail();
					$sender->send_welcome_message($data);
					echo json_encode(array('error'=>FALSE, 'authenticated'=>TRUE, 'message' => 'Welcome!'));
				}
			}
			catch (Exception $e)
			{
				echo json_encode(array('error'=>TRUE, 'authenticated'=>FALSE, 'message' => $e->getMessage()));
			}
		}
		else 
		{

			echo json_encode(array('error'=>TRUE, 'authenticated'=>FALSE, 'message' => 'Something wrong! Please try again!'));
		}	
	}

	function logout()
	{
		$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
		$authenticate->sign_out();
		header('Location: '.$this->_template->helpers->url_for(''));
	}
	
	function login()
	{
		$this->layout_as('ajax');
		$message = 'Something wrong! Please try again!';
		$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
		$is_authenticated = $authenticate->check_user_data();
		if ($this->is_known_form() && !$is_authenticated)
		{
			$data = array();
			if ((isset($_POST['email'])) && (isset($_POST['password'])))
			{
				foreach($_POST as $key=>$value)
				{
					$data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
				}
				$is_authenticated = $authenticate->authenticate($data, FALSE);
				//echo json_encode(array('error'=>FALSE, 'authenticated'=>$is_authenticated));
				
				if(!$is_authenticated)
				{
					$message = 'Email is unkow or Password not match!';
				}
				echo json_encode(array('error'=>FALSE, 'authenticated'=>$is_authenticated, 'message'=>$message));
			}
			else
			{
				$message = 'Email or Password  is empty!';
				echo json_encode(array('error'=>TRUE, 'authenticated'=>FALSE,'message'=>$message));
			}
		} 
		else
		{
			echo json_encode(array('error'=>TRUE, 'authenticated'=>FALSE, 'message'=>$message));
		}
	}

	function save()
	{
		$this->layout_as('ajax');
		$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
		$is_authenticated = $authenticate->check_user_data();
		if ($this->is_known_form() && $this->post_is_known() && $is_authenticated)
		{
			$userModel = new \Application\Models\User();
			try
			{
				$ok = $userModel->save_user($_POST);
				$_SESSION['uid'] = (isset($_POST['value']['email']))? $_POST['value']['email']:$_SESSION['uid'];
				echo json_encode(array('error'=>FALSE, 'authenticated'=>TRUE, 'message' => 'Welcome!'));
			}
			catch (Exception $e)
			{
				echo json_encode(array('error'=>TRUE, 'authenticated'=>FALSE, 'message' => $e->getMessage()));
			}
				
		}
		else 
		{
			echo json_encode(array('error'=>TRUE, 'authenticated'=>FALSE, 'message' => 'Something wrong! Please try again!'));
		}	
	}
	
	function reset_password()
	{
		$this->layout_as('ajax');
		$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
		$is_authenticated = $authenticate->check_user_data();
		if ($this->is_known_form() && $this->post_is_known() && !$is_authenticated)
		{
			try
			{
				$userModel = new \Application\Models\User();
				$userModel->reset_password($_POST);
				$mailHelper = new  \Application\Helpers\SentmailHelper();
				$modal = $mailHelper->resset_password_succsess_modal();
				echo json_encode(array('error'=>FALSE, 'authenticated'=>TRUE, 'message' => 'SUCCSESS!', 'modal' =>$modal ));
			}
			catch (Exception $e)
			{
				echo json_encode(array('error'=>TRUE, 'authenticated'=>FALSE, 'message' => $e->getMessage()));
			}
		}	
	}

}
