<?php
namespace Application\Controllers;

use Application\Models\Index;
use \Exception;

class ContactController extends \Core\AuthenticatedController
{
	function index()
	{
		$this->set('sitePlace', 'contact');
	}
	
	function sent()
	{
		$this->layout_as('ajax');
		if ($this->is_known_form() && $this->post_is_known())
		{
			$sender = new \Application\Models\Sentemail();
			$mailHelper = new  \Application\Helpers\SentmailHelper();
			try
			{
				$sender->send_contact_message($_POST);
				$modal = $mailHelper->contact_succsess_modal();
				echo json_encode(array('error'=>FALSE, 'message' => 'SUCCSESS!', 'modal' => $modal ));
			}
			catch (Exception $e)
			{
				echo json_encode(array('error'=>TRUE, 'message' => $e->getMessage()));
			}
		}	
	}
}
