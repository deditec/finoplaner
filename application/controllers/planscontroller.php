<?php
namespace Application\Controllers;

use Application\Models\Index;

class PlansController extends \Core\AuthenticatedController
{
	function index()
	{
		$planModel = new  \Application\Models\Plan();
		$plans = array();
		if(isset($this->current_user))
		{
			$plans = $planModel->get_plans_for_user($this->current_user['id']);
		}
		else
		{
			$plans = $planModel->get_plans_for_user('demo');
		}
		$this->set('sitePlace', 'plans')->set('plans', $plans);
		
		//$this->render('tryit','comingsoon');
		//$this->_template->helpers->debug($texts);
	}
	
/*
	function view($id = NULL)
	{
		$planModel = new  \Application\Models\Plan();
		$plan = array();
		if($id)
		{
			$this->layout_as('cashadvise_app');
			$plan = $planModel->get_plans_by_id($id);
		}
		$this->set('sitePlace', 'plan')->set('plan', $plan);
	}*/

	
	function action_view($id = NULL){
		$this->layout_as('ajax');
		if($id)
		{
			$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
			$is_authenticated = $authenticate->check_user_data();
			if($is_authenticated)
			{
			 	$planModel = new  \Application\Models\Plan();
				$unit = $planModel->get_plans_by_id($id);
				
				$this->set('plans', $unit);
			}
		}
	}
	
	function save()
	{
		$this->layout_as('ajax');
		if ($this->is_known_form() && $this->post_is_known())
		{
			$planModel = new  \Application\Models\Plan();
			try
			{
				if(isset($this->current_user) && !empty($this->current_user))
				{
					$_POST['value']['user_id'] = $this->current_user['id'];
				}
				$planModel->save_plan($_POST);
				echo json_encode(array('error'=>FALSE, 'message' => 'SUCCSESS!'));
			}
			catch (Exception $e)
			{
				echo json_encode(array('error'=>TRUE, 'message' => $e->getMessage()));
			}
		}
	}
	
	function delete($id = NULL)
	{
		$this->layout_as('ajax');
		if($id)
		{
			$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
			$is_authenticated = $authenticate->check_user_data();
			if($is_authenticated)
			{
			 	$planModel = new  \Application\Models\Plan();
				$planModel->delete_plan($id, $this->current_user['id']);
				
			}
		}
		header('Location: '.BASE_URL.'/plan');
		
	}
	/*
	function demo()
	{
		$this->layout_as('ajax');
		$planModel = new  \Application\Models\Plan();
		$planModel->demo();
	}
	 */
}
