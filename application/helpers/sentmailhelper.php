<?php
namespace Application\Helpers;

use Core\ApplicationHelper;

class SentmailHelper extends \Core\ApplicationHelper
{
	function feedback_succsess_modal()
	{
		return <<<END
<div class="modal hide fade">		
<div class="modal-body">
	<div class="row-fluid">
		<div class="span12">
			<h4 class="greenText">Greed!</h4>
			<p>Thank you for the feedback!</p>
		</div>
		<div class="text-right">
				<a class="btn btn-green" data-dismiss="modal" aria-hidden="true"  > Close </a>
		</div>
	</div>
</div>		
</div>
END;
	}
	
	function resset_password_succsess_modal()
	{
		return <<<END
<div class="modal hide fade">		
<div class="modal-body">
	<div class="row-fluid">
		<div class="span12">
			<h4>You reset your password <span class="greenText">successfully</span>!</h4>
			<p>Pleace check your email for more information!</p>
		</div>
		<div class="text-right">
				<a class="btn btn-green" data-dismiss="modal" aria-hidden="true"  > Close </a>
		</div>
	</div>
</div>		
</div>
END;
	}
	
	function contact_succsess_modal()
	{
		return <<<END
<div class="modal hide fade">		
	<div class="modal-body">
		<div class="row-fluid">
			<div class="span12">
				<h4 class="greenText">Greed!</h4>
				<p>Thank you for the feedback!</p>
			</div>
			<div class="text-right">
					<a class="btn btn-green" data-dismiss="modal" aria-hidden="true"  > Close </a>
			</div>
		</div>
	</div>		
</div>
END;
	}
}
