<?php
namespace Application\Helpers;

use Core\ApplicationHelper;

class FeedbackHelper extends \Core\ApplicationHelper
{
	function feedback_succsess_modal()
	{
		return <<<END
<div class="modal-body">
	<div class="row-fluid">
		<div class="span12">
			<h4 class="greenText">Greed!</h4>
			<p>Thank you for the feedback!</p>
		</div>
		<div class="text-right">
				<a class="btn btn-green" data-dismiss="modal" aria-hidden="true"  > Close </a>
		</div>
	</div>
</div>		
END;
	}
}
