<?php
namespace Application\Helpers;

use Core\ApplicationHelper;

class PlansHelper extends \Core\ApplicationHelper
{
	function print_plans($plans, $authenticated = FALSE)
	{
		$html = '';
		$size = count($plans);
		$count = 0;
		foreach ($plans as $unit) 
		{
			$controlBlok = '';
			if($authenticated)
			{
				$controlBlok = '<div class="control-buttons	">
													<a href="'.$this->url_for('plans/action_view/'.$unit['id']).'" class="summon-modal btn btn-mini" title="Edit" data-remote="true" data-method="post"><i class="icon-pencil"></i></a>
													<a href="'.$this->url_for('plans/delete/'.$unit['id']).'" class="btn btn-mini btn-danger" title="Delete"><i class="icon-cancel"></i></a>
												</div>';
			}
			if($count%2 == 0)
			{
				$html .= '<div class="row-fluid">';
			}
			$html .= '<div class="span6">
							<div class="plan-panel xfake-link" data-href="'.$this->url_for('plan/view/'.$unit['id']).'">
								<div class="text-left">
									<span class="badge badge-warning" >'.($count+1).'</span>
								</div>
								<div class="body">
									 <span>'.$unit['name'].'</span>
								</div>
								<div class="text-right">
									<a href="'.$this->url_for('plan/view/'.$unit['id']).'" class="btn btn-green btn-small">View</a>
								</div>
								'.$controlBlok.'
							</div>	
						</div>';
							
			if($count%2 == 1 || $size-1 == $count)
			{
				$html .= '</div><br />';
			}
		
			$count++;
			
		}

		return $html;
	}
}
