<?php

namespace Application\Models;
use \Exception;

class Sentemail extends \Core\ApplicationModel
{
	static $mailboxs = array(
		'account' => 'account@cashadvise.co',
		'admin' => 'admin@cashadvise.co',
		'feedback' => 'feedback@cashadvise.co',
		'info' => 'info@cashadvise.co',
		'system' => 'system@cashadvise.co'
	);
	
	
	
	static function send_feedback($params)
	{
		if(self::validate_params($params))
		{
			$params =  $params['value']; 
			$screenname = $params['name'];
			$useremail = $params['email'];
			$subject = $params['subject'];
			$text = $params['text'];
			$timestamp = date('j F Y, H:i:s');
			$logo = self::getLogo_link();
			$message_defaul =  <<<END
<h4>This FEEDBACK is be send at {$timestamp} GMT</h4>
===============================================================================
<p>USERNAME: {$screenname}</p>
<p>USEREMAIL: <a href="mailto:{$useremail}">{$useremail}</a></p>
<div>
	<h4>{$subject}</h4>
	<p>{$text}</p>
</div>
===============================================================================
<p>	
	Notice: This is an automatically generated message, please don't reply.
</p>
<p>		
	Thank you!
</p>
<p>
	{$logo}
</p>
END;
			$message = $message_defaul;
				
			$mailUnit = new \PHPMailer();
			$mailUnit->From = $useremail;
			$mailUnit->FromName = $screenname;
			$mailUnit->IsHTML(TRUE);
			$mailUnit->AddAddress(self::$mailboxs["feedback"], '');//
			$mailUnit->Subject = $subject;
			$mailUnit->Body = $message;
			return $mailUnit->Send();
		}
		else 
		{
			return FALSE;
		}
	}

	static function send_contact_message($params)
	{
		if(self::validate_params($params))
		{
			$params =  $params['value']; 
			$screenname = $params['name'];
			$useremail = $params['email'];
			$subject = $params['subject'];
			$text = $params['text'];
			$timestamp = date('j F Y, H:i:s');
			$logo = self::getLogo_link();
			$message_defaul =  <<<END
<h4>This INFOMESSAGE is be send at {$timestamp} GMT</h4>
===============================================================================
<p>USERNAME: {$screenname}</p>
<p>USEREMAIL: <a href="mailto:{$useremail}">{$useremail}</a></p>
<div>
	<h4>{$subject}</h4>
	<p>{$text}</p>
</div>
===============================================================================
<p>	
	Notice: This is an automatically generated message, please don't reply.
</p>
<p>		
	Thank you!
</p>
<p>
	{$logo}
</p>
END;
			$message = $message_defaul;
				
			$mailUnit = new \PHPMailer();
			$mailUnit->From = $useremail;
			$mailUnit->FromName = $useremail;
			$mailUnit->IsHTML(TRUE);
			$mailUnit->AddAddress(self::$mailboxs["info"], '');//
			$mailUnit->Subject = $subject;
			$mailUnit->Body = $message;
			return TRUE; //  $mailUnit->Send();
		}
		else 
		{
			return FALSE;
		}
	}

	static function send_reset_password($user_data)
	{
		$params = array(
			'value' => array(
				'name' => self::$mailboxs["account"],
				'email' => self::$mailboxs["account"],
				'subject' => 'www.cashadvise.co, account system: reset password'
			)
		);
		if(self::validate_params($params))
		{
			$params =  $params['value']; 
			$screenname = $params['name'];
			$systememail = $params['email'];
			$subject = $params['subject'];
			$timestamp = date('j F Y, H:i:s');
			
			$userName = $user_data['name'];
			$newPassword = $user_data['password'];
			$logo = self::getLogo_link();
			$message_defaul =  <<<END
<div>
	<h4>Hello, {$userName}</h4>
	<p>
		You reset your password <span style="color: #6DB802 !important;">successfully</span>! 
		<br />
		The new password is: <strong>{$newPassword}</strong> . Please follow the link: 
		{$logo} and change your password!
	</p>
</div>

<p>	
Notice: This is an automatically generated message, please don't reply.
</p>
<p>		
Thank you!
</p>
<p>
{$logo}
</p>
END;
			$message = $message_defaul;
				
			$mailUnit = new \PHPMailer();
			$mailUnit->From = $systememail;
			$mailUnit->FromName = $screenname;
			$mailUnit->IsHTML(TRUE);
			$mailUnit->AddAddress($user_data['email'], '');//
			$mailUnit->Subject = $subject;
			$mailUnit->Body = $message;
			return $mailUnit->Send();
		}
		else 
		{
			return FALSE;
		}
	}

	static function send_welcome_message($user_data)
	{
		$params = array(
			'value' => array(
				'name' => self::$mailboxs["account"],
				'email' => self::$mailboxs["account"],
				'subject' => 'Welcome to www.cashadvise.co!'
			)
		);
		if(self::validate_params($params))
		{
			$params =  $params['value']; 
			$screenname = $params['name'];
			$systememail = $params['email'];
			$subject = $params['subject'];
			$timestamp = date('j F Y, H:i:s');
			
			$newPassword = $user_data['password'];
			$logo = self::getLogo_link();
			$message_defaul =  <<<END
<div>
	<h4>Welcome to {$logo}</h4>
	<p>
		We are very happy that you became a part of us. 
		Enjoy!!!
	</p>
</div>

<p>	
Notice: This is an automatically generated message, please don't reply.
</p>
<p>		
Thank you!
</p>
<p>
{$logo}
</p>
END;
			$message = $message_defaul;
				
			$mailUnit = new \PHPMailer();
			$mailUnit->From = $systememail;
			$mailUnit->FromName = $screenname;
			$mailUnit->IsHTML(TRUE);
			$mailUnit->AddAddress($user_data['email'], '');//
			$mailUnit->Subject = $subject;
			$mailUnit->Body = $message;
			return $mailUnit->Send();
		}
		else 
		{
			return FALSE;
		}
	}
	
	static function validate_params($params)
	{
		if(!isset($params['value']) || empty($params['value']) )
		{
			throw new Exception('All fields are require!');	
			return FALSE;
		}
		else 
		{
			if(!isset($params['value']['name']) || empty($params['value']['name']) )
			{
				throw new Exception('Enter your name please!');	
				return FALSE;
			}
			
			if(!isset($params['value']['email']) || empty($params['value']['email']) )
			{
				throw new Exception('Enter your email please!');	
				return FALSE;
			}
			
			if(!isset($params['value']['subject']) || empty($params['value']['subject']) )
			{
				throw new Exception('Enter a subject please!');	
				return FALSE;
			}
			
			return TRUE;
		}
	}
	
	static function getLogo()
	{
		return  <<<END
<strong style="
	font-size: 12px;
	font-weight: bold;
	padding: 5px;
	background-color: #333333; 
	border-radius: 3px 3px 3px 3px;
	color: #FFFFFF;
    display: inline-block;
    line-height: 14px;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    vertical-align: baseline;
    white-space: nowrap;
">
	<span style="color: #6DB802 !important;">Cash</span>Advise</strong>
END;
	}
	
	static function getLogo_link()
	{
		$baseURL = BASE_URL;
		return  <<<END
<a href="{$baseURL}" style="text-decoration: none;">
	<strong style="
		font-size: 12px;
		font-weight: bold;
		padding: 5px;
		background-color: #333333; 
		border-radius: 3px 3px 3px 3px;
		color: #FFFFFF;
	    display: inline-block;
	    line-height: 14px;
	    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	    vertical-align: baseline;
	    white-space: nowrap;
	">
		<span style="color: #6DB802 !important;">Cash</span>Advise
	</strong>
</a>
END;
	}
}


