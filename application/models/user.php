<?php

namespace Application\Models;
use \Application\Models;
use \Exception;

class User extends \Core\ApplicationModel
{
	public function get_user_by_mail($email = NULL)
	{
		$this->id = NULL;
		$this->_table = 'Users';
		$this->_model = 'user';
		$this->_extraConditions = array('email = "'.$email.'"');
		$reset_user = $this->find();
		return (isset($reset_user[0]['user']))? $reset_user[0]['user']:array();
	}
	public function save_user($postArray)
	{
		if($this->check_user_data($postArray))
		{
			$this->_table = 'Users';
			if(isset($postArray['userID']) && $postArray['userID'] != 'NULL')
			{
				$this->id = $postArray['userID'];
				unset($postArray['value']['confirm_password']);
				unset($postArray['value']['old_password']);

				if( isset($postArray['value']['password']) )
				{
					$postArray['value']['password'] = $this->set_authKey($postArray['value']['password']);
				}
			}
			else 
			{
				unset($postArray['value']['confirm_password']);
				unset($postArray['value']['agree']);
				$time = time();
				$postArray['value']['password'] = $this->set_authKey($postArray['value']['password']);
				$postArray['value']['name'] = 'user_'.rand(300, $time);
				$postArray['value']['activate'] = date('Y-m-d', $time);
			}
			$this->_describe = $postArray['value'];
			$this->save2();
			$this->check_query_data($this->getErrorNo());

			return $this->getErrorNo();
		}
		
		return empty($postArray['value']);
	}
	
	public function random_password($key)
	{
		$randString = filter_var(md5($key.time()), FILTER_SANITIZE_STRING);
		$PASSWORD_LENGTH = 8;
		return substr($randString, 0, $PASSWORD_LENGTH);
	}
	
	public function reset_password($user_data)
	{
		if(isset($user_data['value']) && isset($user_data['value']['email']) && !empty($user_data['value']['email']))
		{
			
			$reset_user = $this->get_user_by_mail($user_data['value']['email']);
			if(!empty($reset_user))
			{
				$id = $reset_user['id'];
				$newPassword = $this->random_password($reset_user['email']);
				$password = $this->set_authKey($newPassword);
				$this->id = $id;
				$this->_describe = array('password' => $password);
				$this->save2();
				$this->check_query_data($this->getErrorNo());
				if($this->getErrorNo() == 0)
				{
					$reset_user['password'] = $newPassword;
					$sender = new \Application\Models\Sentemail();
					$sender::send_reset_password($reset_user);
					
				}
				else 
				{
					throw new Exception('Somthing wrong! Please try again or contact with support@cashadvise.co');
				}
			}
			else 
			{
				throw new Exception('E-mail is wrong');
			}
		}
		else 
		{
			throw new Exception('E-mail is empty');
		}
	}
	
	public function delete_user($id)
	{
		$this->id = $id;
		$this->_table = 'users';
		$this->delete();
		
		return $this->getError();
	}
	
	private function set_authKey($userpass)
	{
		$security = new \Core\Security();
		$auth_key = $security->encode($userpass);
		return $auth_key;
	}
	
	private function get_authKey($userpass)
	{
		$security = new \Core\Security();
		$auth_key = $security->decode($userpass);
		return $auth_key;
	}
	
	public function check_query_data($errorCode)
	{
		if($errorCode !== 0)
		{
			switch ($errorCode) {
				/* Code for dublicate primery key */
				case 1062:
					throw new Exception('This Email is already used!');	
					break;
				
				default:
					throw new Exception('Something wrong, please try again!');
					break;
			}
		}	
	}

	

	public function check_user_data($user_data)
	{
		
		if(isset($user_data['value']) && !empty($user_data['value']))
		{
			if(isset($user_data['userID']) && !empty($user_data['userID']))
			{
				if(!$this->check_edit_data($user_data))
				{
					return FALSE;
				}
				else
				{
					return TRUE;
				}
			}
			else
			{
				if(!$this->check_signup_data($user_data))
				{
					return FALSE;
				}
				else
				{
					return TRUE;
				}
			}
		}
		else 
		{
			throw new Exception('All fields are require!');
			return FALSE;	
		}


	}

	public function check_signup_data($user_data)
	{
		if(!isset($user_data['value']['email']) || empty($user_data['value']['email']) )
		{
			throw new Exception('Email is empty!');	
			return FALSE;
		}
		if(!isset($user_data['value']['password']) || empty($user_data['value']['password']))
		{
			throw new Exception('Password is empty!');	
			return FALSE;
		}

		if($user_data['value']['confirm_password'] !== $user_data['value']['password'])
		{
			throw new Exception('Password fields not match!');	
			return FALSE;
		}
		
		if(!isset($user_data['value']['agree']) || empty($user_data['value']['agree']))
		{
			throw new Exception('Please, first agree terms and conditions ');	
			return FALSE;
		}
		
		return TRUE;
	}

	public function check_change_password($user_data)
	{
		if($this->set_authKey($user_data['value']['old_password']) !== $this->get_authKey($_SESSION['auth_key']))
		{
				throw new Exception('old password not match!');	
				return FALSE;
		}
		else 
		{
			if(!isset($user_data['value']['password']) || empty($user_data['value']['password']))
			{
				throw new Exception('Password is empty!');	
				return FALSE;
			}

			if($user_data['value']['confirm_password'] !== $user_data['value']['password'])
			{
				throw new Exception('Password fields not match!');	
				return FALSE;
			}
		}

		return TRUE;
	}

	public function check_edit_data($user_data)
	{
		if(!isset($user_data['value']['old_password']))
		{
			if(!isset($user_data['value']['email']) || empty($user_data['value']['email']) )
			{
				throw new Exception('Email is empty!');	
				return FALSE;
			}
			
			if(!isset($user_data['value']['name']) || empty($user_data['value']['name']) )
			{
				throw new Exception('Name is empty!');	
				return FALSE;
			}
		}
		else 
		{
			if(!$this->check_change_password($user_data))
			{
				return FALSE;
			}
			
		}
		return TRUE;
	}
	
}
