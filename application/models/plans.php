<?php

namespace Application\Models;
use \Application\Models;
use \Exception;

class Plans extends \Core\ApplicationModel
{
	/*
	public function get_plans_by_id($id = NULL)
		{
			if($id)
			{
				$query = 'SELECT * FROM Plans WHERE id = "'.$id.'"';
				
				$plans = $this->custom($query);
				$plans = $this->convertArray($plans);
			}		
			return (isset($plans[0]))? $plans[0]:array();
		}*/
	
	
	public function get_plans_for_user($user_id = NULL)
	{
		if($user_id)
		{
			$query = 'SELECT * FROM Plans WHERE user_id = "'.$user_id.'"';
			
			$plans = $this->custom($query);
			$plans = $this->convertArray($plans);
			
		}		
		return (isset($plans))? $plans:array();
	}
	
	public function demo()
	{
			$this->_table = 'Plans';
			$postArray['value']['name'] = 'Demo Plan';
			$postArray['value']['user_id'] = 'demo';
			$this->_describe = $postArray['value'];
			$this->save2();
	}
	
	public function delete_plan($id, $user_id)
	{
		$unit = $this->get_plans_by_id($id);
		if($unit['user_id'] === $user_id)
		{
			$this->id = $id;
			$this->_table = 'Plans';
			$this->delete();
		}
	}
		
	public function save_plan($postArray)
	{
		if($this->check_plan_data($postArray['value']))
		{
			$this->_table = 'Plans';
			if(isset($postArray['value']['id']))
			{
				$this->id = $postArray['value']['id'];
				unset($postArray['value']['id']);
			}
			$this->_describe = $postArray['value'];
			$this->save2();
			$this->check_query_data($this->getErrorNo());
			
			return $this->getErrorNo();
		}
		else 
		{
			return FALSE;
		}
		
		
	}
	
	
	public function check_plan_data($values)
	{
		if(isset($values) && !empty($values))
		{
			if(!isset($values['name']) || empty($values['name']))
			{
				throw new Exception('Name is empty');
				return FALSE;
			}
			
			if(!isset($values['user_id']) || empty($values['user_id']))
			{
				throw new Exception('You must login!');
				return FALSE;
			}
			
			return TRUE;
		}
		else 
		{
			throw new Exception('Name is empty');
			return FALSE;
		}
	}
	
	public function check_query_data($errorCode)
	{
		if($errorCode !== 0)
		{
			switch ($errorCode) {
				/* Code for dublicate primery key */
				case 1062:
					throw new Exception('This Email is already used!');	
					break;
				
				default:
					throw new Exception('Something wrong, please try again!');
					break;
			}
		}	
	}
}
