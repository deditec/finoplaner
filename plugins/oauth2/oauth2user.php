<?php
namespace Plugins\OAuth2;
use \PDO as PDO;

class OAuth2User extends \OAuth2 {

	protected $db;

	public function __construct()
	{
		$this->connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME,DB_TYPE);
	}

	/** Connects to database **/
	function connect($hostname, $username, $password, $dbname, $dbtype)
	{
		$dsn = "{$dbtype}:host={$hostname};dbname={$dbname}";
		try
		{
			$this->db = new PDO($dsn, $username, $password);
			if ($dbtype == 'mysql')
			{
				$this->db->exec("set names utf8");
			}
		}
		catch (PDOException $e)
		{
			$this->db = NULL;
			$this->handleException($e);
			return 0;
		}
	}

	/** Disconnects from database **/
	function disconnect()
	{
		$this->db = null;
	}

	/** Handle PDO exceptional cases. **/

  private function handleException($e) {
    echo "Database error: " . $e->getMessage();
    exit;
  }


	public function addClient($client_email, $client_secret, $redirect_uri) {
    try {
      $sql = "INSERT INTO oauth_clients (client_id, client_email, client_secret, redirect_uri) VALUES (:client_id, client_email, :client_secret, :redirect_uri)";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(":client_id", hash_hmac('sha1', $client_email, APPLICATION_FORGERY), PDO::PARAM_STR);
			$stmt->bindParam(":client_email", $client_email, PDO::PARAM_STR);
      $stmt->bindParam(":client_secret", hash_hmac('sha1', $client_secret, APPLICATION_FORGERY), PDO::PARAM_STR);
      $stmt->bindParam(":redirect_uri", $redirect_uri, PDO::PARAM_STR);
      $stmt->execute();
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

  /**
   * Implements OAuth2::checkClientCredentials().
   *
   * Do NOT use this in production! This sample code stores the secret
   * in plaintext!
   */
  protected function checkClientCredentials($client_id, $client_secret = NULL) {
    try {
      $sql = "SELECT client_secret FROM oauth_clients WHERE client_id = :client_id";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(":client_id", $client_id, PDO::PARAM_STR);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      if ($client_secret === NULL)
          return $result !== FALSE;

      return $result["client_secret"] == hash_hmac('sha1', $client_secret, APPLICATION_FORGERY);
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

  /**
   * Implements OAuth2::getRedirectUri().
   */
  protected function getRedirectUri($client_id) {
    try {
      $sql = "SELECT redirect_uri FROM oauth_clients WHERE client_id = :client_id";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(":client_id", $client_id, PDO::PARAM_STR);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      if ($result === FALSE)
          return FALSE;

      return isset($result["redirect_uri"]) && $result["redirect_uri"] ? $result["redirect_uri"] : NULL;
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

  /**
   * Implements OAuth2::getAccessToken().
   */
  protected function getAccessToken($oauth_token) {
    try {
      $sql = "SELECT client_id, expires, scope FROM oauth_tokens WHERE oauth_token = :oauth_token";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(":oauth_token", $oauth_token, PDO::PARAM_STR);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      return $result !== FALSE ? $result : NULL;
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

  /**
   * Implements OAuth2::setAccessToken().
   */
  protected function setAccessToken($oauth_token, $client_id, $expires, $scope = NULL) {
    try {
			// clean expired tokens for this user first
			$sql = "DELETE FROM oauth_tokens WHERE client_id = :client_id AND expires < :time";
      $stmt = $this->db->prepare($sql);
			$now = time();
			$stmt->bindParam(":client_id", $client_id, PDO::PARAM_STR);
			$stmt->bindParam(":time", $now, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();

			$sql = "INSERT INTO oauth_tokens (oauth_token, client_id, expires, scope) VALUES (:oauth_token, :client_id, :expires, :scope)";
      $stmt2 = $this->db->prepare($sql);
      $stmt2->bindParam(":oauth_token", $oauth_token, PDO::PARAM_STR);
      $stmt2->bindParam(":client_id", $client_id, PDO::PARAM_STR);
      $stmt2->bindParam(":expires", $expires, PDO::PARAM_INT);
      $stmt2->bindParam(":scope", $scope, PDO::PARAM_STR);

      $stmt2->execute();
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

	/**
   * Implements OAuth2::getRefreshToken().
   */
  protected function getRefreshToken($refresh_token) {
    try {
      $sql = "SELECT refresh_token as token, client_id, expires, scope FROM oauth_refresh_tokens WHERE refresh_token = :refresh_token";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(":refresh_token", $refresh_token, PDO::PARAM_STR);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      return $result !== FALSE ? $result : NULL;
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

	/**
   * Implements OAuth2::setRefreshToken().
   */
  protected function setRefreshToken($refresh_token, $client_id, $expires, $scope = NULL) {
    try {
			$sql = "DELETE FROM oauth_refresh_tokens WHERE client_id = :client_id AND expires < :time";
      $stmt = $this->db->prepare($sql);
			$now = time();
			$stmt->bindParam(":client_id", $client_id, PDO::PARAM_STR);
			$stmt->bindParam(":time", $now, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();

      $sql = "INSERT INTO oauth_refresh_tokens (refresh_token, client_id, expires, scope) VALUES (:refresh_token, :client_id, :expires, :scope)";
      $stmt2 = $this->db->prepare($sql);
      $stmt2->bindParam(":refresh_token", $refresh_token, PDO::PARAM_STR);
      $stmt2->bindParam(":client_id", $client_id, PDO::PARAM_STR);
      $stmt2->bindParam(":expires", $expires, PDO::PARAM_INT);
      $stmt2->bindParam(":scope", $scope, PDO::PARAM_STR);

      $stmt->execute();
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }


  /**
   * Implements OAuth2::unsetRefreshToken().
   */
  protected function unsetRefreshToken($refresh_token) {
    try {
      $sql = "DELETE FROM oauth_refresh_tokens WHERE refresh_token = :refresh_token";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(":refresh_token", $refresh_token, PDO::PARAM_STR);
      $stmt->execute();
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

  /**
   * Overrides OAuth2::getSupportedAuthResponseTypes().
   */
	protected function getSupportedAuthResponseTypes() {
    return array(
      OAUTH2_AUTH_RESPONSE_TYPE_AUTH_CODE
    );
  }

  /**
   * Overrides OAuth2::getSupportedGrantTypes().
   */
  protected function getSupportedGrantTypes() {
    return array(
      OAUTH2_GRANT_TYPE_AUTH_CODE,
			OAUTH2_GRANT_TYPE_REFRESH_TOKEN
    );
  }

  /**
   * Overrides OAuth2::getAuthCode().
   */
  protected function getAuthCode($code) {
    try {
      $sql = "SELECT code, client_id, redirect_uri, expires, scope FROM oauth_auth_codes WHERE code = :code";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(":code", $code, PDO::PARAM_STR);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      return $result !== FALSE ? $result : NULL;
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }

  /**
   * Overrides OAuth2::setAuthCode().
   */
  protected function setAuthCode($code, $client_id, $redirect_uri, $expires, $scope = NULL) {
    try {
			// clean expired auth codes for this user first
			$sql = "DELETE FROM oauth_auth_codes WHERE client_id = :client_id AND expires < :time";
      $stmt = $this->db->prepare($sql);
			$now = time();
			$stmt->bindParam(":client_id", $client_id, PDO::PARAM_STR);
			$stmt->bindParam(":time", $now, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();

      $sql = "INSERT INTO oauth_auth_codes (code, client_id, redirect_uri, expires, scope) VALUES (:code, :client_id, :redirect_uri, :expires, :scope)";
      $stmt2 = $this->db->prepare($sql);
      $stmt2->bindParam(":code", $code, PDO::PARAM_STR);
      $stmt2->bindParam(":client_id", $client_id, PDO::PARAM_STR);
      $stmt2->bindParam(":redirect_uri", $redirect_uri, PDO::PARAM_STR);
      $stmt2->bindParam(":expires", $expires, PDO::PARAM_INT);
      $stmt2->bindParam(":scope", $scope, PDO::PARAM_STR);

      $stmt2->execute();
    } catch (PDOException $e) {
      $this->handleException($e);
    }
  }
}
