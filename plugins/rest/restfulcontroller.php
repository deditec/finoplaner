<?php
namespace Plugins\REST;
class RESTfulController extends \Core\ApplicationController{

	protected $_method;
	protected $_accept;
	protected $_arguments;
	protected $_supportedMethods = array('GET','HEAD','POST','PUT','DELETE');

  public function __construct($controller, $action, $module="default")
	{
		$this->_method = $_SERVER['REQUEST_METHOD'];
		$this->_accept = $_SERVER['HTTP_ACCEPT'];
		switch ($this->_method) {
			case 'GET':
				$action = 'get';
				$this->_arguments = $_GET;
			break;
			case 'HEAD':
				$action = 'head';
				$this->_arguments = $_GET;
			break;
			case 'POST':
				$action = 'post';
				$this->_arguments = $_POST;
			break;
			case 'PUT':
				$action = 'put';
				parse_str(file_get_contents('php://input'), $this->_arguments);
			break;
			case 'DELETE':
				$action = 'delete';
				parse_str(file_get_contents('php://input'), $this->_arguments);
			break;
			default:
				$this->methodNotAllowedResponse();
			break;
		}
		parent::__construct($controller, $action, $module);
  }

	public function index()
	{
		switch ($this->_method) {
			case 'GET':
				$this->get();
			break;
			case 'HEAD':
				$this->head();
			break;
			case 'POST':
				$this->post();
			break;
			case 'PUT':
				$this->put();
			break;
			case 'DELETE':
				$this->delete();
			break;
			default:
				$this->methodNotAllowedResponse();
			break;
		}
	}

	public function get()
	{
		$this->methodNotAllowedResponse();
	}
	public function head()
	{
		$this->methodNotAllowedResponse();
	}
	public function post()
	{
		$this->methodNotAllowedResponse();
	}
	public function put()
	{
		$this->methodNotAllowedResponse();
	}
	public function delete()
	{
		$this->methodNotAllowedResponse();
	}

  protected function methodNotAllowedResponse() {
    header('Allow: ' . $this->_supportedMethods, true, 405);
  }

}
