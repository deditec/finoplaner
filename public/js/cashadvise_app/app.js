$(function(){
	var $body = $('body');
	
	$body.delegate('#plan-form','ajax:complete', function(data, xhr){
		var response = $.parseJSON(xhr.responseText);
		if ((response.error == false)) 
		{
			location.reload();
		}
		else
		{
			$('.alert', this).html('<button type="button" class="close" data-dismiss="alert">×</button>'+response.message).fadeIn();
			$('input[type="password"]', this).val('');
		}
		$('.ajaxToggle', this).toggle();
		$('[type=submit]', this).removeAttr('disabled');
	});
	
	$body.delegate('#playgrounds-form, #signup-form, #login-form, #editUser-form, #chanagePassword-form','ajax:complete', function(data, xhr){
		var response = $.parseJSON(xhr.responseText);
		if ((response.error == false) && (response.authenticated == true)) 
		{
			location.reload();
		}
		else
		{
			$('.alert', this).html('<button type="button" class="close" data-dismiss="alert">×</button>'+response.message).fadeIn();
			$('input[type="password"]', this).val('');
		}
		$('.ajaxToggle', this).toggle();
		$('[type=submit]', this).removeAttr('disabled');
	});
	
	$body.delegate('#feedback-form, #contact-form, #forgote-password-form','ajax:complete', function(data, xhr){
		//console.log(xhr.responseText);
		var response = $.parseJSON(xhr.responseText);
		if ((response.error == false)) 
		{
			//location.reload();
			var $modal = response.modal;
	  		var $modalBox = $('.modal-container');
		  	if($modalBox.length)
		  	{
		  		$('.modal').modal('hide');
		 		$modalBox.empty().html($modal).removeClass('hide');
		 		$('.modal').modal();
		 		$('input', this).val(''); 		
		  	}
		}
		else
		{
			$('.alert', this).html('<button type="button" class="close" data-dismiss="alert">×</button>'+response.message).fadeIn();

		}
		$('.ajaxToggle', this).toggle();
		$('[type=submit]', this).removeAttr('disabled');
	});
	
	
	createStoryJS({
        type:       'timeline',
        width:      '100%',
        height:     '600',
        source: 	'https://docs.google.com/spreadsheet/pub?key=0Agl_Dv6iEbDadHdKcHlHcTB5bzhvbF9iTWwyMmJHdkE&amp;output=html',
        embed_id:   'my-timeline'           // ID of the DIV you want to load the timeline into
    });
	
});

