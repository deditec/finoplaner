/* LOAD jQuery*/
(function ($) {
  	$(function(){
	  		/*
	  		 * Activete rails js for ajax calls
	  		 * how to ? <a href="" data-remote="true" data-method="post">
	  		 * events: ajax:before; ajax:error; ajax:complete
	  		 * 
	  		 * $(body).delegate('foo',' ajax:complete',function(e){})
	  		 * */
	  		$.rails.allowAction = function(element) {
			var message = element.data('confirm'), answer = false, callback;
			if (!message) { return true; }
	
			if ($.rails.fire(element, 'confirm')) {
				showConfirmationModal(message, function() {
					callback = $.rails.fire(element, 'confirm:complete', [answer]);
					if(callback) {
					  var oldAllowAction = $.rails.allowAction;
					  $.rails.allowAction = function() { return true; };
					  element.trigger('click');
					  $.rails.allowAction = oldAllowAction;
					}
				});
			}
			return false;
		};
		/*activete carousel*/
		$('.carousel').carousel({
	  		interval: 8000
		});
		
		/*activete tooltip*/
		$('[data-toggle="tooltip"]').tooltip();
		
		var $body = $('body');
		/* summon modal window  :) */
		summon_modals($body);
		
		/*
		 Show/Hide content
		 * use class .showContent
		 * use attibute data-show (for show content)
		 * use attibute data-hide (for hide content)
		 */
		$body.delegate('.showContent', 'click', function(e) {
		  var $this = $(this);
		  var $showContent = $('.'+$this.attr('data-show'));
		  var $hideContent = $('.'+$this.attr('data-hide'));
		  show_hide_content($showContent, $hideContent)
		});
		
		$body.delegate('.fake-link', 'click', function  (e) {
		    var $this = $(this);
		    window.location.href = $this.attr('data-href');
		})
  	});
  
	function show_hide_content($showObj, $hideObj)
	{
		$hideObj.hide();
		$showObj.fadeIn();
	}

	function summon_modals($body){
	  	$body.delegate('.summon-modal','click', function(event) {
	  		$(this).blur();
		});
	
		$body.delegate('.summon-modal','ajax:complete', function(xhr, data) {
	  		var $modal = data.responseText;
	  		var $modalBox = $('.modal-container');
	  		if($modalBox.length)
	  		{
	 			$modalBox.empty().html($modal).removeClass('hide');
	 			$('.modal').modal(); 		
	  		}
	});
}
  
})(jQuery);