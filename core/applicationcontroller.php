<?php
namespace Core;

class ApplicationController
{
	protected $_module;
	protected $_controller;
	protected $_action;
	protected $_template;
	protected $request_is_ajax = FALSE;

	private $disableLayout;
	private $render;

	function __construct($controller, $action, $module='application')
	{
		global $inflect;

		$this->_module = $module;
		$this->_controller = ucfirst($controller);
		$this->_action = $action;

		$model_name = $inflect->singularize($controller);
		$model = ucfirst($model_name);
		$helper = ucfirst($controller).'Helper';
		if (!file_exists(APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . $controller.'helper.php'))
		{
			$this->$helper = new \Core\ApplicationHelper;
		}
		else
		{
			$helper_with_ns = ucfirst($module) . '\\Helpers\\' . $helper;
			$this->$helper = new $helper_with_ns;
		}
		
		
		$this->_template = new Template($controller,$action,$module);
		$this->_template->set_helpers($this->$helper);
		$this->_template->set('helpers', $this->$helper);
		if (file_exists(APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR. 'models' . DIRECTORY_SEPARATOR . $model_name . '.php'))
		{
			$model_with_ns = ucfirst($module) . '\\Models\\' . ucfirst($model_name);
			$this->$model = new $model_with_ns;
		}


		$this->render = TRUE;
		$this->disableLayout = FALSE;
		$this->check_ajax();
	}
	
	function check_ajax()
	{
		if ((isset($_SERVER['HTTP_X_REQUESTED_WITH'])) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'))
		{
			$this->request_is_ajax = TRUE;
			$this->layout_as('ajax');
		}
		$this->set('request_is_ajax',$this->request_is_ajax);
		return $this->request_is_ajax;
	}


	function set_render($render)
	{
		$this->render = $render;
	}

	function disable_layout()
	{
		$this->disableLayout = TRUE;
		return $this;
	}

	function set($name,$value)
	{
		$this->_template->set($name,$value);
		return $this;
	}

	function layout_as($layout)
	{
		$this->_template->set_layout($layout);
		return $this;
	}

	function render($controller, $action)
	{
		$this->_template->render($this->disableLayout,$controller, $action);
		$this->set_render(FALSE);
		return $this;
	}

	function __destruct()
	{
		if ($this->render)
		{
			$this->_template->render($this->disableLayout);
		}
	}

	function request_is_post()
	{
		return (isset($_POST['csrf']));
	}

	function post_is_known()
	{
		return TRUE;
		//return (((isset($_POST['csrf'])) && ($_POST['csrf'] == CSRF)) || ((isset($_SERVER['HTTP_X_CSRF_TOKEN'])) && ($_SERVER['HTTP_X_CSRF_TOKEN'] == CSRF)));
	}

	function referer_is_known()
	{
		return (boolean) preg_match("%^".BASE_URL."%",$_SERVER['HTTP_REFERER']);
	}

	function is_known_form()
	{
		return TRUE;
		//return ($this->request_is_post() && $this->post_is_known() && $this->referer_is_known());
	}

	function sanitize($value)
	{
		return filter_var($value, FILTER_SANITIZE_STRING);
	}
		
}
