<?php
namespace Core;

class Cache 
{

	function get($fileName) 
	{
		$fileName = APPLICATION_BASE . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $fileName;
		if (file_exists($fileName)) 
		{
			$handle = fopen($fileName, 'rb');
			$variable = fread($handle, filesize($fileName));
			fclose($handle);
			return unserialize($variable);
		} 
		else 
		{
			return null;
		}
	}

	function is_expired($fileName, $expiration_limit) 
	{
		$fileName = APPLICATION_BASE . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $fileName;
		if (file_exists($fileName)) 
		{
			$lastmod = time() - filemtime($fileName);
			return ($lastmod > $expiration_limit);
		}
		return FALSE;
	}

	function set($fileName,$variable) 
	{
		$fileName = APPLICATION_BASE . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $fileName;
		$handle = fopen($fileName, 'w');
		fwrite($handle, serialize($variable));
		fclose($handle);
	}
	
}
