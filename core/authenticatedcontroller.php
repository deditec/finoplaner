<?php
namespace Core;

use \Core\ApplicationController;

class AuthenticatedController 
	extends ApplicationController
{

	function before_action()
	{
		$authenticate = new \Core\UserAuth($this->_controller, $this->_action, $this->_module);
	
		$is_authenticated = $authenticate->is_authenticated();
		
		$this->set('authenticated', $is_authenticated);
		if ($is_authenticated)
		{
			$this->current_user = $authenticate->get_user_data();
			$this->set('current_user', $this->current_user);
		}
		return $is_authenticated;
	
	}
}
