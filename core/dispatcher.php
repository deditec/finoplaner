<?php
@session_start();
header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
date_default_timezone_set('Europe/London');

/** Check if environment is development and display errors **/
function set_reporting()
{
	if (DEVELOPMENT_ENVIRONMENT === TRUE)
	{
		error_reporting(E_ALL);
		ini_set('display_errors','On');
	}
	else
	{
		error_reporting(E_ALL);
		ini_set('display_errors','Off');
		ini_set('log_errors', 'On');
		ini_set('error_log', APPLICATION_BASE . DIRECTORY_SEPARATOR . 'tmp'. DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'error.log');
	}
}

/** Check register globals and remove them **/

function unregister_globals() 
{
  if (ini_get('register_globals')) 
  {
      $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
      foreach ($array as $value) 
      {
				if (!empty($GLOBALS[$value]))
				{
          foreach ($GLOBALS[$value] as $key => $var) 
          {
              if ((isset($GLOBALS[$key])) && ($var === $GLOBALS[$key]) && (!in_array($key,array('url','default','routing','inflect','irregularWords')))) 
              {
                  unset($GLOBALS[$key]);
              }
          }
				}
      }
  }
}

/** Secondary Call Function **/

function perform_action($controller,$action,$queryString = null,$render = 0)
{
	$controllerName = ucfirst($controller).'Controller';
	$dispatch = new $controllerName($controller,$action);
	$dispatch->render = $render;
	return call_user_func_array(array($dispatch,$action),$queryString);
}

/** Routing **/

function route_url($url)
{
	global $routing;
	foreach ( $routing as $pattern => $result )
	{
		if (preg_match($pattern, $url))
		{
			return preg_replace($pattern, $result, $url);
		}
	}
	return ($url);
}

/** Main Call Function **/

function call_hook()
{
	global $url;
	global $default;

	$queryString = array();

	if (!isset($url))
	{
		$module = $default['module'];
		$controller = $default['controller'];
		$action = $default['action'];
	}
	else
	{
		$url = route_url($url);
		$urlArray = array();
		$urlArray = explode("/",$url);
		$module = $urlArray[0];
		if ((strtolower($module) == $default['module']) || (is_dir(APPLICATION_BASE  . DIRECTORY_SEPARATOR. $module)))
		{
			array_shift($urlArray);
		}
		else 
		{
			$module = $default['module'];
		}

		if (!empty($urlArray[0]))
		{
			$controller = $urlArray[0];
			array_shift($urlArray);
		}
		else
		{
			$controller = $default['controller'];
		}

		if (!empty($urlArray[0]))
		{
			$action = $urlArray[0];
			array_shift($urlArray);
		}
		else
		{
			$action = $default['action'];
		}
		$queryString = $urlArray;
	}

	$controllerName = '\\' . ucfirst($module) . '\\Controllers\\'. ucfirst($controller).'Controller';
	$dispatch = new $controllerName($controller,$action,$module);
	if ((int)method_exists($controllerName, $action))
	{
		if((int)method_exists($controllerName,'before_action'))
		{
			call_user_func_array(array($dispatch,"before_action"),$queryString);
		}
		call_user_func_array(array($dispatch,$action),$queryString);
		if((int)method_exists($controllerName,'after_action'))
		{
			call_user_func_array(array($dispatch,"after_action"),$queryString);
		}
	}
	else
	{
		/* Error Generation Code Here */
		header("HTTP/1.0 404 Not Found");
		$dispatch->set_render(FALSE);
		$url = '/'.$url;
		include_once (APPLICATION_BASE . DIRECTORY_SEPARATOR . STATIC_DIR_NAME . DIRECTORY_SEPARATOR . '404.php');
	}
}

/** Autoload any classes that are required **/

function class_loader($className)
{
	$className = ltrim($className, '\\');
	$namespaced = explode('\\', $className);
	$fileName = '';

	if (!empty($namespaced))
	{
		$className = array_pop($namespaced);
		if (!empty($namespaced))
		{
			$fileName = strtolower(implode(DIRECTORY_SEPARATOR, $namespaced)) . DIRECTORY_SEPARATOR;
		}
	}
	$fileName .= strtolower(str_replace('_', DIRECTORY_SEPARATOR, $className));

	if (file_exists(APPLICATION_BASE . DIRECTORY_SEPARATOR . 'external'. DIRECTORY_SEPARATOR . $fileName . '.php'))
	{
		require(APPLICATION_BASE . DIRECTORY_SEPARATOR . 'external' . DIRECTORY_SEPARATOR . $fileName . '.php');
		return TRUE;
	}

	if (file_exists(APPLICATION_BASE . DIRECTORY_SEPARATOR . $fileName . '.php'))
	{
		require(APPLICATION_BASE . DIRECTORY_SEPARATOR . $fileName . '.php');
		return TRUE;
	}

	global $url;
	/* Error Generation */
	$url = '/'.$url;
	if (!preg_match('/Controller/',$className))
	{
		header("HTTP/1.0 500 Internal Server Error");
		include_once (APPLICATION_BASE . DIRECTORY_SEPARATOR . STATIC_DIR_NAME . DIRECTORY_SEPARATOR . '500.php');
	}
	else
	{
		header("HTTP/1.0 404 Not Found");
		include_once (APPLICATION_BASE . DIRECTORY_SEPARATOR . STATIC_DIR_NAME . DIRECTORY_SEPARATOR . '404.php');
	}
	exit();
}


/** GZip Output **/

function gzip_output()
{
	$ua = $_SERVER['HTTP_USER_AGENT'];
	if (0 !== strpos($ua, 'Mozilla/4.0 (compatible; MSIE ') || false !== strpos($ua, 'Opera'))
	{
		return false;
	}
	$version = (float)substr($ua, 30);
	return (($version < 6) || ($version == 6  && false === strpos($ua, 'SV1')));
}

/** Determine the default module */
function get_module()
{
	global $url;
	global $default;

	if (!isset($url)) 
	{
		$module = $default['module'];
	}
	else
	{
		$url = route_url($url);
		$urlArray = array();
		$urlArray = explode("/", $url);
		$module = $urlArray[0];
	}
	if (!is_dir(APPLICATION_BASE . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR. $module))
	{
		$module = null;
	}

	$scopedAppPath = APPLICATION_BASE . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR;
	if ((!empty($module)) && (is_dir($scopedAppPath . $module)))
	{
		$scopedAppPath .= $module . DIRECTORY_SEPARATOR;
	}
	define ('MODULE_PATH', $scopedAppPath);
	define ('CURRENT_MODULE', $module);
	forgery();
}

/** CSRF */
function forgery()
{
	if (!isset($_SESSION['CSRF']))
	{
		$time = time();
		$_SESSION['CSRF'] = hash_hmac('md5',$time,APPLICATION_FORGERY);
	}
	define('CSRF', $_SESSION['CSRF']);
}

/** Get Required Files **/
get_module();
//gzip_output() || ob_start("ob_gzhandler");
spl_autoload_register('class_loader');
$cache = new \Core\Cache();
$inflect = new \Core\Inflection();
set_reporting();
unregister_globals();
call_hook();

?>
