<?php
namespace Core;

class UserAuth 
	extends MySQLQuery
{
	private $_security = NULL;
	private $_login = NULL;

	function __construct($controller, $action, $module='default')
	{
		$this->_security = new Security();
		$this->_login = BASE_URL.'/user';
		parent::__construct();
		/*
		global $url;
		$this->_module = $module;
		$this->_controller = strtolower($controller);
		$this->_action = $action;
		//parent::__construct($controller, $action, $module);
		$this->_security = new \Core\Security();
		$this->_login = BASE_URL . '/'. $this->_module . '/' . AUTH_LOGIN_CONTROLLER . '/' . AUTH_LOGIN_ACTION;
		*/
	}

	function get_collection()
	{
		/*
		$db = $this->get_database();
		$collection = $db->users;
		$collection->ensureIndex(array("email" => 1), array("unique" => true));
		*/
		return $this;
	}

	function is_authenticated($redirect = TRUE)
	{
		
		$ok = $this->check_user_data();
		/*
		if (!$ok)
		{
			$data = array();
			if ((isset($_POST['email'])) && (isset($_POST['password'])))
			{
				foreach($_POST as $key=>$value)
				{
					$data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
				}
				$ok = $this->authenticate($data, $redirect);
			}
			else
			{
				if ($redirect)
				{
					header('Location: '.$this->_login);
				}
			}
		}
		
		*/
		return $ok;
	}

	function check_user_data()
	{
		
		if ((isset($_SESSION['auth_key'])) && (!empty($_SESSION['auth_key'])))
		{
			$auth_key = $this->_security->decode($_SESSION['auth_key']);
			$user_id = $_SESSION['uid'];
			$query = 'SELECT * FROM Users WHERE password="'.$auth_key.'" AND email="'.$user_id.'"';
			$user = $this->custom($query);
			//$total = $this->get_collection()->find(array('auth_key' => $auth_key, '_id' => new MongoId($user_id)))->count(TRUE);
			if (count($user) == 1)
			{
				
				$item = $user[0]['User'];
				return ((''.$item['password'] === $auth_key) && (''.$item['email'] === $user_id));
			}
			else
			{
				/*
				$items = $this->get_collection()->find(array('auth_key' => $auth_key));
				$ids = array();
				foreach ($items as $item)
				{
					array_push($ids, new MongoId($item['_id']));
				}
				$conditions = array('_id'=>array('$in'=>$ids));
				$this->get_collection()->update($conditions,array('auth_key'=>''));
				*/
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	function sign_out()
	{
		if ((isset($_SESSION['auth_key'])) && (!empty($_SESSION['auth_key'])))
		{
			unset($_SESSION['auth_key']);
			unset($_SESSION['uid']);
			/*
			$item = $this->get_collection()->findOne(array('auth_key' => $_SESSION['auth_key'], '_id'=>new MongoId($_SESSION['uid'])));
			$item['auth_key'] = '';
			$item['online'] = FALSE;
			$conditions = array('_id' => $item['_id']);
			$this->get_collection()->update($conditions,$item);
			*/
		}
		return TRUE;
	}

	function authenticate($data, $redirect = TRUE)
	{
		$data['password'] = $this->_security->encode($data['password']); 
		/* code pass */
		//$item = $this->get_collection()->findOne(array('email' => $data['email'], 'password' => hash_hmac('sha1',$data['password'],APPLICATION_FORGERY)));
		$query = 'SELECT * FROM Users WHERE email="'.$data['email'].'" AND password="'.$data['password'].'" LIMIT 1';
		$user = $this->custom($query);
		if (!empty($user))
		{
			$item = $user[0]['User'];
			$auth_key = $this->_security->encode($item['password']);
			$item['auth_key'] = $auth_key;
			$uid = (string) $item['email'];
			$_SESSION['auth_key'] = $auth_key;
			$_SESSION['uid'] = $uid;
			return TRUE;
			/*
			
			$item['auth_key'] = $auth_key;
			$item['online'] = TRUE;
			$uid = (string) $item['_id'];
			$conditions = array('_id'=>new MongoId($item['_id']));
			$this->get_collection()->update($conditions,$item);
			$_SESSION['auth_key'] = $auth_key;
			$_SESSION['uid'] = $uid;
			return TRUE;
			*/
		}
		else
		{
			if ($redirect === TRUE)
			{
				header('Location: '.$this->_login);
			}
			return FALSE;
			
		}
		return FALSE;
	}

	function get_user_data()
	{
		$query = 'SELECT Users.*, T.typename FROM Users LEFT JOIN Acc_types T ON Users.type=T.code  WHERE Users.password="'.$this->_security->decode($_SESSION['auth_key']).'" AND Users.email="'.$_SESSION['uid'].'"';
		$user = $this->custom($query);
		unset($user[0]['User']['password']);
		$user[0]['User']['typename'] = $user[0]['T']['typename'];
		return $user[0]['User'];
	}
}
