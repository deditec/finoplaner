<?php
namespace Core;

class ApplicationModel 
	extends MySQLQuery 
{
	function d($what = '!@#$'){
		echo '<pre style="margin: 4px; padding: 4px; background: #f0f0f0; color: #111111; border: 1px solid #111111;">';
		echo htmlentities(print_r($what, true), ENT_QUOTES, 'UTF-8');
		echo '</pre>';
	}
	
	function convertArray($re_array = array())
	{
		$new_array = array();
		
		if(!empty($re_array))
		{
			foreach ($re_array as $unit) 
			{
				foreach ($unit as $unit2) 
				{
					array_push($new_array, $unit2);
				}
				
			}		
		}
		//$this->d($new_array);
		return $new_array;
	}
}
