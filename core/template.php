<?php
namespace Core;

class Template 
{
	
	protected $variables = array();
	protected $_module;
	protected $_controller;
	protected $_action;
	protected $_layout = NULL;
	
	public $content;
	public $helpers;
	
	function __construct($controller,$action,$module='application') 
	{
		$this->_module = $module;
		$this->_controller = $controller;
		$this->_action = $action;
		
	}

	/** Set Variables **/

	function set($name,$value)
	{
		$this->variables[$name] = $value;
	}

	function set_helpers($helper)
	{
		$this->helpers = $helper;
	}

	function set_layout($layout)
	{
		$this->_layout = $layout;
	}

	function get_view($filename)
	{
		extract($this->variables);
		if (is_file($filename))
		{
			ob_start();
			include $filename;
			$contents = ob_get_contents();
			ob_end_clean();
			return $contents;
		}
		return false;
	}

	/** Display Template **/
	
	function render($disableLayout = FALSE, $controller = NULL, $action = NULL)
	{
		extract($this->variables);
		$controller = ($controller !== NULL) ? $controller : $this->_controller;
		$action = ($action !== NULL) ? $action : $this->_action;
 
		if (file_exists(APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $controller . DIRECTORY_SEPARATOR . $action . '.php')) 
		{
			$this->content = $this->get_view (APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $controller . DIRECTORY_SEPARATOR . $action . '.php');
		}

		if ($disableLayout === FALSE)
		{
			if ($this->_layout === NULL)
			{
				$this->_layout = $controller;
			}
			if (file_exists(APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'views'. DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . $this->_layout . '.php')) 
			{
				include (APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'views'. DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . $this->_layout . '.php');
			} 
			else 
			{
				include (APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'views'. DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'application.php');
			}
		}
		else
		{
			echo $this->content;
		}
  }

	function render_partial($partial, $variables=array())
	{
		$vars = array_merge($this->variables, $variables);
		extract($vars);

		if (file_exists(APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR . $partial . '.php')) 
		{
			ob_start();
			include (APPLICATION_BASE . DIRECTORY_SEPARATOR . $this->_module . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR . $partial . '.php');
			$contents = ob_get_contents();
			ob_end_clean();
			echo $contents;
		}
  }

}
