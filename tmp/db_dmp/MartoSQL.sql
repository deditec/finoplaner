-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Време на генериране: 
-- Версия на сървъра: 5.5.27
-- Версия на PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `cashadvice`
--

-- --------------------------------------------------------

--
-- Структура на таблица `actions`
--

CREATE TABLE IF NOT EXISTS `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `action_type` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `action_items`
--

CREATE TABLE IF NOT EXISTS `action_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `action_type` int(11) DEFAULT NULL,
  `transfer_section_id` int(11) DEFAULT NULL,
  `due_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `action_id` (`action_id`),
  KEY `transfer_section_id` (`transfer_section_id`),
  KEY `due_id` (`due_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `action_item_rows`
--

CREATE TABLE IF NOT EXISTS `action_item_rows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_item_id` int(11) NOT NULL,
  `target_section_id` int(11) DEFAULT NULL,
  `option` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `percent` double DEFAULT NULL,
  `remaining` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `action_item_id` (`action_item_id`),
  KEY `target_section_id` (`target_section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `calendar_item`
--

CREATE TABLE IF NOT EXISTS `calendar_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Ссхема на данните от таблица `calendar_item`
--

INSERT INTO `calendar_item` (`id`, `name`) VALUES
(1, 'Day'),
(2, 'Week'),
(3, 'Month'),
(4, 'Year');

-- --------------------------------------------------------

--
-- Структура на таблица `cards`
--

CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `due_id` int(11) DEFAULT NULL,
  `payment_credit_id` int(11) DEFAULT NULL,
  `apr` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`),
  KEY `due_id` (`due_id`),
  KEY `payment_credit_id` (`payment_credit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `day_of_week`
--

CREATE TABLE IF NOT EXISTS `day_of_week` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Ссхема на данните от таблица `day_of_week`
--

INSERT INTO `day_of_week` (`id`, `name`) VALUES
(1, 'Monday'),
(2, 'Tuesday'),
(3, 'Wednesday'),
(4, 'Thursday'),
(5, 'Friday'),
(6, 'Saturday'),
(7, 'Sunday');

-- --------------------------------------------------------

--
-- Структура на таблица `day_option`
--

CREATE TABLE IF NOT EXISTS `day_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Ссхема на данните от таблица `day_option`
--

INSERT INTO `day_option` (`id`, `day_name`) VALUES
(1, 'Exact'),
(2, 'Before weekend'),
(3, 'After weekend'),
(4, 'Closer to weekend');

-- --------------------------------------------------------

--
-- Структура на таблица `duration`
--

CREATE TABLE IF NOT EXISTS `duration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calendar_item_id` int(11) DEFAULT NULL,
  `calendar_count` int(11) DEFAULT NULL,
  `day_option_id` int(11) DEFAULT NULL,
  `business_days` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_item_id` (`calendar_item_id`),
  KEY `day_option_id` (`day_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `loans`
--

CREATE TABLE IF NOT EXISTS `loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `measured_by` int(11) DEFAULT NULL,
  `duration_id` int(11) DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `periodic_id` int(11) DEFAULT NULL,
  `payment_credit_id` int(11) DEFAULT NULL,
  `fixed_amount_type` tinyint(1) DEFAULT NULL,
  `fixed_amount` double DEFAULT NULL,
  `init_amount` double DEFAULT NULL,
  `final_amount` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`),
  KEY `duration_id` (`duration_id`),
  KEY `periodic_id` (`periodic_id`),
  KEY `payment_credit_id` (`payment_credit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `periodic`
--

CREATE TABLE IF NOT EXISTS `periodic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `periodic_day`
--

CREATE TABLE IF NOT EXISTS `periodic_day` (
  `id` int(11) NOT NULL,
  `periodic_id` int(11) DEFAULT NULL,
  `days_count` int(11) NOT NULL,
  `day_option_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `periodic_id` (`periodic_id`),
  KEY `day_option_id` (`day_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `periodic_month`
--

CREATE TABLE IF NOT EXISTS `periodic_month` (
  `id` int(11) NOT NULL,
  `periodic_id` int(11) DEFAULT NULL,
  `month_count` int(11) NOT NULL,
  `option` int(11) DEFAULT NULL,
  `exact_day` int(11) DEFAULT NULL,
  `rel_week_id` int(11) DEFAULT NULL,
  `rel_dayofweek_id` int(11) DEFAULT NULL,
  `rel_day_id` int(11) DEFAULT NULL,
  `rel_day_option_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `periodic_id` (`periodic_id`),
  KEY `rel_week_id` (`rel_week_id`),
  KEY `rel_dayofweek_id` (`rel_dayofweek_id`),
  KEY `rel_day_id` (`rel_day_id`),
  KEY `rel_day_option_id` (`rel_day_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `periodic_week`
--

CREATE TABLE IF NOT EXISTS `periodic_week` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periodic_id` int(11) DEFAULT NULL,
  `weeks_count` int(11) NOT NULL,
  `day_of_week_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `periodic_id` (`periodic_id`),
  KEY `day_of_week_id` (`day_of_week_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `plans`
--

CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `range_location`
--

CREATE TABLE IF NOT EXISTS `range_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Ссхема на данните от таблица `range_location`
--

INSERT INTO `range_location` (`id`, `name`) VALUES
(1, 'First'),
(2, 'Last');

-- --------------------------------------------------------

--
-- Структура на таблица `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `section_type` int(11) DEFAULT NULL,
  `start_date` date NOT NULL,
  `initial_balance` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура на таблица `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`);

--
-- Ограничения за таблица `action_items`
--
ALTER TABLE `action_items`
  ADD CONSTRAINT `action_items_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`),
  ADD CONSTRAINT `action_items_ibfk_2` FOREIGN KEY (`transfer_section_id`) REFERENCES `sections` (`id`),
  ADD CONSTRAINT `action_items_ibfk_3` FOREIGN KEY (`due_id`) REFERENCES `periodic` (`id`);

--
-- Ограничения за таблица `action_item_rows`
--
ALTER TABLE `action_item_rows`
  ADD CONSTRAINT `action_item_rows_ibfk_1` FOREIGN KEY (`action_item_id`) REFERENCES `action_items` (`id`),
  ADD CONSTRAINT `action_item_rows_ibfk_2` FOREIGN KEY (`target_section_id`) REFERENCES `sections` (`id`);

--
-- Ограничения за таблица `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
  ADD CONSTRAINT `cards_ibfk_2` FOREIGN KEY (`due_id`) REFERENCES `periodic` (`id`),
  ADD CONSTRAINT `cards_ibfk_3` FOREIGN KEY (`payment_credit_id`) REFERENCES `duration` (`id`);

--
-- Ограничения за таблица `duration`
--
ALTER TABLE `duration`
  ADD CONSTRAINT `duration_ibfk_1` FOREIGN KEY (`calendar_item_id`) REFERENCES `calendar_item` (`id`),
  ADD CONSTRAINT `duration_ibfk_2` FOREIGN KEY (`day_option_id`) REFERENCES `day_option` (`id`);

--
-- Ограничения за таблица `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
  ADD CONSTRAINT `loans_ibfk_2` FOREIGN KEY (`duration_id`) REFERENCES `duration` (`id`),
  ADD CONSTRAINT `loans_ibfk_3` FOREIGN KEY (`periodic_id`) REFERENCES `periodic` (`id`),
  ADD CONSTRAINT `loans_ibfk_4` FOREIGN KEY (`payment_credit_id`) REFERENCES `duration` (`id`);

--
-- Ограничения за таблица `periodic_day`
--
ALTER TABLE `periodic_day`
  ADD CONSTRAINT `periodic_day_ibfk_1` FOREIGN KEY (`periodic_id`) REFERENCES `periodic` (`id`),
  ADD CONSTRAINT `periodic_day_ibfk_2` FOREIGN KEY (`day_option_id`) REFERENCES `day_option` (`id`);

--
-- Ограничения за таблица `periodic_month`
--
ALTER TABLE `periodic_month`
  ADD CONSTRAINT `periodic_month_ibfk_1` FOREIGN KEY (`periodic_id`) REFERENCES `periodic` (`id`),
  ADD CONSTRAINT `periodic_month_ibfk_2` FOREIGN KEY (`rel_week_id`) REFERENCES `range_location` (`id`),
  ADD CONSTRAINT `periodic_month_ibfk_3` FOREIGN KEY (`rel_dayofweek_id`) REFERENCES `day_of_week` (`id`),
  ADD CONSTRAINT `periodic_month_ibfk_4` FOREIGN KEY (`rel_day_id`) REFERENCES `range_location` (`id`),
  ADD CONSTRAINT `periodic_month_ibfk_5` FOREIGN KEY (`rel_day_option_id`) REFERENCES `day_option` (`id`);

--
-- Ограничения за таблица `periodic_week`
--
ALTER TABLE `periodic_week`
  ADD CONSTRAINT `periodic_week_ibfk_1` FOREIGN KEY (`periodic_id`) REFERENCES `periodic` (`id`),
  ADD CONSTRAINT `periodic_week_ibfk_2` FOREIGN KEY (`day_of_week_id`) REFERENCES `day_of_week` (`id`);

--
-- Ограничения за таблица `plans`
--
ALTER TABLE `plans`
  ADD CONSTRAINT `plans_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения за таблица `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
