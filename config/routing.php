<?php

$routing = array(
	'/admin\/(.*?)\/(.*?)\/(.*)/' => 'admin/\1_\2/\3'
);

$default['module'] = 'application';
$default['controller'] = 'index';
$default['action'] = 'index';